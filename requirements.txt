colour-science
ipykernel
matplotlib==3.6.3
numba
numpy==1.23.5
opencv-contrib-python==4.7.0.72
opencv-contrib-python-headless==4.6.0.66
openwholeslide
packaging
pandas==1.5.3
Pillow==9.4.0
pydegensac==0.1.2
scikit-image==0.19.3
scikit-learn==1.2.1
tqdm==4.64.1