# Setting global ARGs enable to access their values in any of the latter stage (cf. https://github.com/moby/moby/issues/38379#issuecomment-447835596)

# For a base image with cuda (11.7.1), nvcc (devel) and cuDNN (cudnn8) use the following base image: nvidia/cuda:11.7.1-cudnn8-devel-ubuntu22.04
ARG IMAGE=ubuntu:jammy

ARG USERNAME=feature
ARG USER_UID=1000
ARG USER_GID=$USER_UID

FROM $IMAGE

LABEL maintainer="Arthur Elskens <arthur.elskens@ulb.be>"
LABEL description="The Dockerfile to build an image used to run the feature-based registration pipeline."

ARG USERNAME
ARG USER_UID
ARG USER_GID

# Create user that correspond to the user on the OS
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
	&& usermod --shell /bin/bash $USERNAME \
    #
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

USER $USERNAME
ENV USER_HOME="/home/${USERNAME}"

# Fix ownership issue with the home directory
RUN sudo chown -R ${USERNAME} ${USER_HOME}

# ARG WKDIR="/home/${USERNAME}/src"
# WORKDIR ${WKDIR}

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

# Get build dependencies
ENV DEBIAN_FRONTEND=noninteractive
RUN sudo apt-get update \
	&& sudo apt-get install -y \
		build-essential \
		software-properties-common \
	    python-is-python3 \
        python3.10-venv \
        python3-pip \
        git \
		wget \
        cmake \
		ca-certificates

# Create and activate a venv
ENV VIRTUAL_ENV="${USER_HOME}/opt/feature"
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

ENV DEBIAN_FRONTEND=noninteractive

ARG OPENCV=false
ARG VFC=false

# Build OpenCV from source
RUN if [ "${OPENCV}" = "true" ] || [ "${VFC}" = "true" ] ; \
	then mkdir ~/opencv_build && cd ~/opencv_build \
	&& git clone https://github.com/opencv/opencv.git \
	&& git clone https://github.com/opencv/opencv_contrib.git \
	&& cd ~/opencv_build/opencv \
	&& mkdir -p build && cd build \
	&& cmake -D CMAKE_BUILD_TYPE=RELEASE \
		-D CMAKE_INSTALL_PREFIX=/usr/local \
		-D INSTALL_C_EXAMPLES=OFF \
		-D INSTALL_PYTHON_EXAMPLES=OFF \
		-D OPENCV_GENERATE_PKGCONFIG=ON \
		-D OPENCV_EXTRA_MODULES_PATH=~/opencv_build/opencv_contrib/modules \
		-D BUILD_EXAMPLES=OFF .. \
	&& make -j8 \
	&& sudo make install ; \
	fi

# For Python's OpenCV version 
RUN sudo apt-get update \
	&& sudo apt-get install -y libgl1

# Install VFC
RUN if [ ${VFC} = true ] ; \
	then cd ${WKDIR} \
	&& git clone https://github.com/aelskens/pyvfc.git \
	&& pip install ./pyvfc ; \
	fi

ARG WKDIR="${USER_HOME}/src"
WORKDIR $WKDIR

# Install the repository at WORKDIR location
COPY . .

# Install all extra package for this project
RUN pip install -r requirements.txt
RUN pip install -r dev-requirements.txt

ENV PYTHONPATH="${WKDIR}:${WKDIR}/src/feature-based_registration"