"""
This module is used to run the experiments and to extend them.
"""

import argparse
import logging
import os
from typing import List

import tqdm
from datasets.anhir import ANHIRDataset
from datasets.generic import CompleteDataset
from datasets.marzahl import ISCDataset, MSSCDataset
from experiment import FeatureBasedExperiment
from extend_experiments import extend_preproc_supv_outputs
from processing.channel_reduction import grayscale
from registration.descriptors import AVAILABLE_DESCRIPTORS, Descriptor
from registration.detectors import AVAILABLE_DETECTORS, Detector
from registration.features_extraction import ModularDetectionDescription
from registration.features_matching import ModularMatchingFiltering
from registration.filtering_methods import AVAILABLE_RANSACS, RANSACFamily
from registration.matching_methods import BruteForce
from skimage.transform import AffineTransform


def create_parser() -> argparse.ArgumentParser:
    """Create the arg parser.
    For addition information: https://docs.python.org/3/library/argparse.html

    :return parser: The arg parser
    :rtype: argparse.ArgumentParser
    """

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o",
        "--outpath",
        type=str,
        required=True,
        default=None,
        help="The existing directory where the outputs of each experiment should be saved.",
    )
    parser.add_argument(
        "-l",
        "--output-level",
        type=str,
        nargs="+",
        required=False,
        default="NOT_LOGS",
        help="The list of outputs to save for each experiment.",
    )
    parser.add_argument(
        "--generate_analysis",
        dest="generate_analysis",
        action="store_true",
        help="Whether to generate the analysis outputs or not.",
    )

    return parser


def main(basepath: str, output_level: List[str]) -> None:
    """Run the experiments and extend them.

    :param basepath: The path where the experiments should be saved.
    :type basepath: str
    :param output_level: The list of outputs to save for each experiment.
    :type output_level: List[str]
    """

    to_compare = {
        "preproc": [
            ("Gray", grayscale, {"invert": False, "as_ubyte": True}),
        ],
        "desc": [
            ("Random", Descriptor(method=AVAILABLE_DESCRIPTORS["Random"]), "norm_l2"),
            ("Neighborhood", Descriptor(method=AVAILABLE_DESCRIPTORS["Neighborhood"]), "norm_l2"),
            ("SIFT", Descriptor(method=AVAILABLE_DESCRIPTORS["SIFT"]), "norm_l2"),
            ("VGG", Descriptor(method=AVAILABLE_DESCRIPTORS["VGG"], scale_factor=5.0), "norm_l2"),
            ("BRISK", Descriptor(method=AVAILABLE_DESCRIPTORS["BRISK"]), "norm_hamming"),
            ("FREAK", Descriptor(method=AVAILABLE_DESCRIPTORS["FREAK"]), "norm_hamming"),
            ("BRIEF", Descriptor(method=AVAILABLE_DESCRIPTORS["BRIEF"]), "norm_hamming"),
        ],
    }

    ds = CompleteDataset(
        datasets=[
            ANHIRDataset("/data/dataset_ANHIR"),
            MSSCDataset("/data/marzahl/MSSC"),
            ISCDataset("/data/marzahl/ISC"),
        ]
    )

    print("Start the experiments.")
    for pair in tqdm.tqdm(ds.dev_set.values(), total=len(ds.dev_set), desc="Pairs"):
        for name, method, kwargs in to_compare["preproc"]:
            preproc_path = os.path.join(basepath, name)

            if not os.path.exists(preproc_path):
                os.mkdir(preproc_path)

            for n, desc, dist in tqdm.tqdm(
                to_compare["desc"], total=len(to_compare["desc"]), desc="Descriptors", leave=False
            ):
                outpath = os.path.join(preproc_path, n)

                if not os.path.exists(outpath):
                    os.mkdir(outpath)

                try:
                    exp = FeatureBasedExperiment(
                        src_wsi=pair["src"],
                        dst_wsi=pair["dst"],
                        outpath=outpath,
                        src_gt_path=pair["src_gt"],
                        dst_gt_path=pair["dst_gt"],
                        output_level=output_level,
                        magnification=1.0,
                        preprocessing_cls=method,
                        preprocessing_kwargs=kwargs,
                        features_cls=ModularDetectionDescription,
                        features_kwargs={
                            "detector": Detector(method=AVAILABLE_DETECTORS["SIFT"]),
                            "descriptor": desc,
                        },
                        matching_cls=ModularMatchingFiltering,
                        matching_kwargs={
                            "match_method": BruteForce(distance=dist, cross_check=True),
                            "filter_method": RANSACFamily(
                                method=AVAILABLE_RANSACS["affRANSAC"],
                                transform=AffineTransform,
                                ransacReprojThreshold=7,
                            ),
                        },
                    )

                    exp.execute()
                    exp.evaluate(extract_gt=pair["gt_extractor"])

                except Exception as e:
                    with open(os.path.join(basepath, "failed_exp.log"), "a", encoding="utf-8") as log:
                        log.write("#" * 140 + "\n")
                        log.write(f"{exp.dir_path} failed.\n")
                        log.write(
                            f"For pair: {os.path.basename(pair['src'].path)} - {os.path.basename(pair['dst'].path)}.\n"
                        )
                        log.write(f"Error: {e}\n")

    # Clear logger to avoid writing in the last logfile
    logger = logging.getLogger()
    logger.handlers.clear()

    print("Extend the experiments with the masks and ranks.")
    for name, method, kwargs in to_compare["preproc"]:
        preproc_path = os.path.join(basepath, name)
        extend_preproc_supv_outputs(preproc_path)


if __name__ == "__main__":
    parser = create_parser()
    args = parser.parse_args()

    main(basepath=args.outpath, output_level=args.output_level)

    if args.generate_analysis:
        from generate_tables_and_figures import main as generate_tables_and_figures

        generate_tables_and_figures(exps_path=args.outpath)
