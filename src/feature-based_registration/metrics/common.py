import itertools as it
from numbers import Number
from typing import Any, Callable, List, Tuple, TypeVar, Union

import numpy as np
import scipy as sp
import scipy.stats as st

T = TypeVar("T", bound=Number)


def compute_interquartile(values: np.ndarray | List[float]) -> Tuple[float]:
    """Compute the interquartile range (IQ range) of the given distribution.

    :param values: The distribution from which to compute the IQ range.
    :type values: np.ndarray | List[float]
    :return: The IQ range of the given distribution expressed as: (p25, p75).
    :rtype: Tuple[float]
    """

    return (np.percentile(values, 25), np.percentile(values, 75))


def compute_descriptive_statistics(
    values: np.ndarray | List[float], measures: List[Callable] = [np.median, compute_interquartile]
) -> List[Union[int, float, Tuple[T, T]]]:
    """Compute the descriptive statistics of a distribution.

    :param values: The distribution from which to compute the descriptive statistics.
    :type values: np.ndarray | List[float]
    :param measures: The descriptive statistics to compute on the given distribution. The options are
    any descriptive statistic callable that accepts as input the distribution (e.g. np.mean, np.median,
    np.std, ...), defaults to [np.median, compute_interquartile].
    :type measures: List[Callable], optional
    :return: The list of descriptive statistics in the same order as the one given by the ``measures``
    argument.
    :rtype: List[Union[int, float, Tuple[T, T]]]
    """

    out = []
    for measure in measures:
        out.append(measure(values))

    return out


def friedman_test(*args: Any) -> Tuple[float, float, List[float], List[float]]:
    """Performs a Friedman ranking test. Tests the hypothesis that in a set of k dependent samples groups
    (where k >= 2) at least two of the groups represent populations with different median values.

    From: I. Rodríguez-Fdez, A. Canosa, M. Mucientes, A. Bugarín, STAC: a web platform for the comparison of
    algorithms using statistical tests, in: Proceedings of the 2015 IEEE International Conference on Fuzzy
    Systems (FUZZ-IEEE), 2015. (https://github.com/citiususc/stac)

    References:
    M. Friedman, The use of ranks to avoid the assumption of normality implicit in the analysis of variance,
        Journal of the American Statistical Association 32 (1937) 674-701.
    D.J. Sheskin, Handbook of parametric and nonparametric statistical procedures. crc Press, 2003,
        Test 25: The Friedman Two-Way Analysis of Variance by Ranks

    :param sample1, sample2, ... : The sample measurements for each group.
    :type sample1, sample2, ...: array_like
    :raises ValueError: Raised when less than two samples are provided.
    :raises ValueError: Raised when the samples' sizes are not equal.
    :return iman_davenport: The computed F-value of the test.
    :rtype iman_davenport: float
    :return p-value: The associated p-value from the F-distribution.
    :rtype p-value: float
    :return rankings: The ranking for each group.
    :rtype rankings: List[float]
    :return pivots: The pivotal quantities for each group.
    :rtype pivots: List[float]
    """

    k = len(args)
    if k < 2:
        raise ValueError("Less than 2 levels")
    n = len(args[0])
    if len(set([len(v) for v in args])) != 1:
        raise ValueError("Unequal number of samples")

    rankings = []
    for i in range(n):
        row = [col[i] for col in args]
        row_sort = sorted(row)
        rankings.append([row_sort.index(v) + 1 + (row_sort.count(v) - 1) / 2.0 for v in row])

    rankings_avg = [sp.mean([case[j] for case in rankings]) for j in range(k)]
    rankings_cmp = [r / sp.sqrt(k * (k + 1) / (6.0 * n)) for r in rankings_avg]

    chi2 = ((12 * n) / float((k * (k + 1)))) * (
        (sp.sum(r**2 for r in rankings_avg)) - ((k * (k + 1) ** 2) / float(4))
    )
    iman_davenport = ((n - 1) * chi2) / float((n * (k - 1) - chi2))

    p_value = 1 - st.f.cdf(iman_davenport, k - 1, (k - 1) * (n - 1))

    return iman_davenport, p_value, rankings_avg, rankings_cmp


def nemenyi_multitest(pivots: List[float]) -> np.ndarray:
    """Performs a Nemenyi post-hoc test using the pivot quantities obtained by a ranking test. Tests the
    hypothesis that the ranking of each pair of groups are different.

    From: I. Rodríguez-Fdez, A. Canosa, M. Mucientes, A. Bugarín, STAC: a web platform for the comparison of
    algorithms using statistical tests, in: Proceedings of the 2015 IEEE International Conference on Fuzzy
    Systems (FUZZ-IEEE), 2015. (https://github.com/citiususc/stac)

    References:
    Bonferroni-Dunn: O.J. Dunn, Multiple comparisons among means, Journal of the American Statistical
        Association 56 (1961) 52-64.

    :param pivots: A list with the pivotal quantity for each group.
    :type pivots: List[float]
    :return pvArray: The associated p-values from the Z-distribution as a symmetrical 2D array.
    :rtype: np.ndarray
    """

    k = len(pivots)
    versus = list(it.combinations(range(k), 2))
    pvArray = np.zeros((k, k))
    m = int(k * (k - 1) / 2.0)

    for vs in versus:
        z = abs(pivots[vs[0]] - pivots[vs[1]])
        pv = 2 * (1 - st.norm.cdf(abs(z)))
        pvArray[vs[0], vs[1]] = min(m * pv, 1)
        pvArray[vs[1], vs[0]] = min(m * pv, 1)

    return pvArray
