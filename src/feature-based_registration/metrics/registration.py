import logging
from typing import Optional

import numpy as np
from metrics.utils import convert_pixel_to_physical_units
from openwholeslide import FloatVector, VectorType
from profiling.decorators import with_logging
from profiling.utils import get_heading


class DimensionError(Exception):
    """Inappropriate dimensions of an input (of correct type)."""


def euclidean_norm(
    src: np.ndarray, dst: np.ndarray, pixel_mapping: VectorType = FloatVector(x=1.0, y=1.0)
) -> np.ndarray:
    """Computing the Euclidean norm for each keypoint/landmark pair.

    :param src: Set of src keypoints/landmarks.
    :type  src: np.ndarray
    :param dst: Set of dst keypoints/landmarks.
    :type  src: np.ndarray
    :param pixel_mapping: The size, in both the x and y axis, of a pixel.
    :type pixel_mapping: VectorType, optional
    :raises TypeError: When either src or dst is of NoneType.
    :raises DimensionError: Miss-match between src and dst dimensions.
    :raises Exception: When either src or dst is empty.
    :return eucl_norm: The distances between the two sets in
    pixels, sqrt(sum((dst-src)**2)).
    :rtype eucl_norm: np.ndarray
    """

    if src is None or dst is None:
        raise TypeError(
            f"Type of src: {type(src)} and dst: {type(dst)}, while neither src nor dst can be of NoneType."
        )

    if np.asarray(src).shape[1:] != np.asarray(dst).shape[1:]:
        raise DimensionError(
            "Miss-match between src and dst dimensions: src.shape[1:] "
            f"= {np.asarray(src).shape[1:]} and dst.shape[1:] = {np.asarray(dst).shape[1:]}"
        )

    # Determine the maximal number of keypoints/landmarks in common between
    # src and dst. This is to make sure that only the common elements are used
    # in the Euclidean norm computation.
    nb_common = min(len(pts) for pts in [src, dst])
    if nb_common <= 0:
        raise Exception(
            "Not possible to compute the Euclidean norm, either src "
            f"(len={len(src)}) or dst (len={len(dst)}) is empty."
        )

    src = np.asarray(src)[:nb_common]
    dst = np.asarray(dst)[:nb_common]
    # eucl_norm = np.linalg.norm(convert_pixel_to_physical_units(dst - src, pixel_mapping), axis=1) # To verify
    eucl_norm = np.sqrt(np.sum(np.power(convert_pixel_to_physical_units(dst - src, pixel_mapping), 2), axis=1))

    return eucl_norm


@with_logging(heading=get_heading(__name__, parent_folder=True))
def tre(src: np.ndarray, dst: np.ndarray, pixel_mapping: VectorType = FloatVector(x=1.0, y=1.0)) -> np.ndarray:
    """Computing Target Registration Error (TRE) for each keypoint/landmark pair.

    :param src: Set of src keypoints/landmarks.
    :type  src: np.ndarray
    :param dst: Set of dst keypoints/landmarks.
    :type  src: np.ndarray
    :param pixel_mapping: The size, in both the x and y axis, of a pixel.
    :type pixel_mapping: VectorType, optional
    :return: The remaining errors between the src and dst keypoints/landmarks in
    pixels, sqrt(sum((dst-src)**2)).
    :rtype: np.ndarray
    """

    return euclidean_norm(src, dst, pixel_mapping=pixel_mapping)


@with_logging(heading=get_heading(__name__, parent_folder=True))
def rmse(
    src: np.ndarray,
    dst: Optional[np.ndarray] = None,
    pixel_mapping: VectorType = FloatVector(x=1.0, y=1.0),
) -> float:
    """Compute the Root Mean Square Error (RMSE) between the two sets of keypoints/landmarks.

    :param src: Set of src keypoints/landmarks.
    :type  src: np.ndarray
    :param dst: Set of dst keypoints/landmarks. If dst is ``None`` then src is considered to already
    contain the result of the euclidean norm, defaults to None.
    :type dst: Optional[np.ndarray], optional
    :param pixel_mapping: The size, in both the x and y axis, of a pixel.
    :type pixel_mapping: VectorType, optional
    :return rmse: The RMSE between the two sets in pixels, sqrt(sum(diffs**2)/len(diffs)).
    :rtype rmse: float
    """

    diffs = src

    if dst is not None:
        logging.debug("Compute the euclidean norm between src and dst.")
        diffs = euclidean_norm(src, dst, pixel_mapping=pixel_mapping)

    rmse = np.sqrt(np.sum(np.power(diffs, 2)) / len(diffs))

    return rmse
