"""
Module containing methods to reduce RGB images to a single channel for further
processing.
"""
from __future__ import annotations

from typing import Optional

import numpy as np
from processing.stain import rgb2stain
from scipy.interpolate import Akima1DInterpolator
from skimage import img_as_ubyte
from skimage.color import rgb2gray
from skimage.exposure import rescale_intensity


def grayscale(im: np.ndarray, invert: bool, as_ubyte: bool = False):
    gray = rgb2gray(im)
    if as_ubyte:
        gray = img_as_ubyte(gray)
    if invert:
        return gray.max() - gray
    return gray


def haematoxylin(
    im: np.ndarray,
    white: Optional[np.ndarray] = None,
    white_percentile: int = 99,
    invert: bool = False,
    as_ubyte: bool = False,
) -> np.ndarray:
    """Extract the hematoxylin channel in the given image.

    :param im: The image from which the hematoxylin channel will be extracted.
    :type im: np.ndarray
    :param white: The estimation of the background "white" color. If nothing is provided
    the white is automatically estimated with the extract_white function, defaults to None.
    :type white: Optional[np.ndarray], optional
    :param white_percentile: The percentile of the brightness distribution to take as threshold. This
    argument is left aside if the white argument is not None, defaults to 99.
    :type white_percentile: int, optional
    :param invert: Whether resulting image should be inverted or not, defaults to False.
    :type invert: bool, optional
    :param as_ubyte: Whether the image should be encoded as uint8 or not, defaults to False.
    :return: bool, optional
    :rtype: np.ndarray
    """

    hem = rgb2stain(im, white=white, white_percentile=white_percentile)[..., 0]

    if invert:
        hem = 255 - hem

    if not as_ubyte:
        hem = rescale_intensity(hem, out_range=np.float32)

    return hem


def get_channel_stats(img: np.ndarray) -> np.ndarray:
    return np.array(
        [np.percentile(img, 1), np.percentile(img, 5), np.mean(img), np.percentile(img, 95), np.percentile(img, 99)]
    )


def norm_img_stats(
    src: np.ndarray, dst: np.ndarray, src_mask: Optional[np.ndarray] = None, dst_mask: Optional[np.ndarray] = None
) -> np.ndarray:
    """Normalize the src image with respect to the dst image statistics.

    Based on method in
    "A nonlinear mapping approach to stain normalization in digital histopathology images using
    image-specific color deconvolution.", Khan et al. 2014

    :param src: The image to normalize, should be encoded in 8 bits (value range = [0, 255]).
    :type src: np.ndarray
    :param dst: The image used to normalize src, should be encoded in 8 bits (value range = [0, 255]).
    :type dst: np.ndarray
    :param src_mask: A mask that will be applied to the src image to restrain the area from which the
    statistics will be computed on, defaults to None.
    :type src_mask: Optional[np.ndarray], optional
    :param dst_mask: A mask that will be applied to the dst image to restrain the area from which the
    statistics will be computed on, defaults to None.
    :type dst_mask: Optional[np.ndarray], optional
    :return src_normed: The normalized src image with respect to the dst image statistics.
    :rtype: np.ndarray
    :return dst: The dst image.
    :rtype: np.ndarray
    """

    stats_flat = []
    for i, m in zip([dst, src], [dst_mask, src_mask]):
        if m is None:
            stats_flat.append(get_channel_stats(i))
        else:
            stats_flat.append(get_channel_stats(i[m > 0]))

    dst_stats_flat, src_stats_flat = stats_flat

    # Avoid duplicates and keep in ascending order
    lower_knots = np.array([0])
    upper_knots = np.array([300, 350, 400, 450])
    src_stats_flat = np.hstack([lower_knots, src_stats_flat, upper_knots]).astype(float)
    dst_stats_flat = np.hstack([lower_knots, dst_stats_flat, upper_knots]).astype(float)

    # Add epsilon to avoid duplicate values
    eps = 10 * np.finfo(float).resolution
    eps_array = np.arange(len(src_stats_flat)) * eps
    src_stats_flat = src_stats_flat + eps_array
    dst_stats_flat = dst_stats_flat + eps_array

    # Make sure src stats are in ascending order
    src_order = np.argsort(src_stats_flat)
    src_stats_flat = src_stats_flat[src_order]
    dst_stats_flat = dst_stats_flat[src_order]

    cs = Akima1DInterpolator(src_stats_flat, dst_stats_flat)

    if src_mask is None:
        src_normed = cs(src.reshape(-1)).reshape(src.shape)
    else:
        src_normed = src.copy()
        src_normed[src_mask > 0] = cs(src[src_mask > 0])

    if src.dtype == np.uint8:
        src_normed = np.clip(src_normed, 0, 255)

    return src_normed, dst
