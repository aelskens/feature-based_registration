from dataclasses import dataclass, field
from typing import Any, Optional

import numpy as np
from profiling.decorators import benchmark, with_logging
from profiling.utils import get_heading
from skimage.transform import ProjectiveTransform, warp


@dataclass
class Warping2D:
    """Warping class that allow the warping of a set of points in 2D.

    :param transform: The instantiation of a skimage-like transformation
    class that will be used to warp the data. It should be callable and
    implement an inverse method to get the inverse transform.
    :type transform: ProjectiveTransform
    """

    exec_time: Optional[float] = field(init=False, repr=False, default=None)

    transform: ProjectiveTransform

    @with_logging()
    def warp_img(self, img: np.ndarray, **kwargs: Any) -> np.ndarray:
        """Warp the image according to the given transformation parameters.

        :param img: The image to warp.
        :type img: np.ndarray
        :return: The warped image according to the given transformation.
        :rtype: np.ndarray
        """

        return warp(img, self.transform.inverse, **kwargs)

    @with_logging()
    def warp_pts(self, pts: np.ndarray) -> np.ndarray:
        """Warp the keypoints/landmarks according to the given transformation parameters.

        :param pts: The keypoints/landmarks coordinates to warp.
        :type pts: np.ndarray
        :return: The warped keypoints/landmarks.
        :rtype: np.ndarray
        """

        return self.transform(pts)

    @with_logging(heading=get_heading(__name__))
    @benchmark
    def __call__(self, data: np.ndarray, **kwargs: Any) -> np.ndarray:
        """Warp the data according to the given transformation parameters.

        :param data: The data to warp, either a set of keypoints/landmarks
        coordinates (shape = N x 2) or an image (shape = M x P).
        :type data: np.ndarray
        :return: The warped data according to the given transformation.
        :rtype: np.ndarray
        """

        if data.shape[1] > 2:
            return self.warp_img(data, **kwargs)

        return self.warp_pts(data)
