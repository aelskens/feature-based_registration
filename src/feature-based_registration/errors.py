class PathNotADirectoryError(ValueError):
    """Custom error that is raised when the given path is not a directory."""

    def __init__(self, *, path: str) -> None:
        self.path = path

    def __str__(self) -> None:
        return f"{self.path} is not a valid directory path."
