import io
import logging
import re
from typing import Any, Callable, Dict, List, Optional, Tuple, Union

import numpy as np
from matplotlib import cm, colors
from matplotlib import pyplot as plt
from matplotlib.axes import Axes
from PIL import Image
from profiling.decorators import with_logging
from profiling.utils import get_heading
from skimage.exposure import equalize_adapthist, rescale_intensity
from viz.colormaps import color_multichannel, get_n_colors, jzazbz_cmap

ArrayLike = np.typing.ArrayLike

logger = logging.getLogger()


def compute_point_size_from_img_dim(img_size: np.ndarray, composite: int = 1) -> Optional[int]:
    """Compute the size of each point that will be drawn based on the size of the image.

    :param img: The size of the image on which the point will be drawn.
    :type img: np.ndarray
    :param composite: The number of 'normal-sized' images that compose the given image, defaults to 1.
    :type composite: int, optional
    :return size: The size of the points to draw.
    :rtype: Optional[int]
    """

    dims = (img_size[0] * img_size[1]) / composite

    size = 2
    for t in [i * 1e5 for i in range(5, 1005, 5)]:
        if dims < t:
            return size
        size += 1

    return


@with_logging(heading=get_heading(__name__, parent_folder=True))
def draw_overlap_img(images: List[np.ndarray], inverted: bool = True) -> np.ndarray:
    """Draw an image showing the overlap of images.

    :param images: The list of images to overlap.
    :type images: List[np.ndarray]
    :param inverted: Whether the image should be inverted before overlapping them.
    :type inverted: bool, optional
    :return overlap_img: The resulting overlapping image.
    :rtype overlap_img: np.ndarray
    """

    MAX_DTYPES = {
        "uint8": 255,
        "float16": 1.0,
        "float32": 1.0,
        "float64": 1.0,
        "float128": 1.0,
    }

    if inverted:
        images = [MAX_DTYPES[str(img.dtype)] - img for img in images]

    composite_img = np.dstack(images)
    cmap = jzazbz_cmap()
    channel_colors = get_n_colors(cmap, composite_img.shape[2])
    overlap_img = color_multichannel(
        composite_img, channel_colors, rescale_channels=True, normalize_by="channel", cspace="CAM16UCS"
    )

    overlap_img = equalize_adapthist(overlap_img)
    overlap_img = rescale_intensity(overlap_img, out_range=(0, 255)).astype(np.uint8)

    return overlap_img


def compute_optimal_fig_size(img_size: Optional[np.ndarray], figsize_max: float = 20.0) -> Tuple[float]:
    """Compute the optimal fig_size with respect to the size of the given image and the maximal
    figure size parameter.

    :param img_size: The dimensions of the image.
    :type img_size: Optional[np.ndarray]
    :param figsize_max: The maximal figsize in inches, defaults to 18.0.
    :type figsize_max: float, optional
    :return fig_size: The figure and axes from matplotlib.pyplot.subplots().
    :rtype: Tuple[float]
    """

    if img_size is None or not list(img_size):
        return (figsize_max, figsize_max)

    size = np.array(img_size[:2])
    fig_size = size[::-1] / float(size.max()) * figsize_max

    return fig_size


@with_logging(heading=get_heading(__name__, parent_folder=True))
def draw_init_warped_target(
    ax: Axes,
    src_img: np.ndarray,
    dst_img: np.ndarray,
    src_pts: np.ndarray,
    warped_src_pts: np.ndarray,
    dst_pts: np.ndarray,
    colors: List[str] = ["green", "blue", "red"],
    circle_size: Union[str, int] = "auto",
    fill_circle: bool = False,
    figsize_max: float = 20.0,
    legend: bool = True,
    legend_kwargs: Dict[str, Any] = {"fontsize": "x-large"},
) -> None:
    """Draw the initial, warped and target keypoints/landmarks over the image overlay.

    :param ax: A matplotlib.axes.Axes used to display the drawn images and keypoints/landmarks.
    :type ax: Axes
    :param src_img: The src image.
    :type src_img: np.ndarray
    :param dst_img: The dst image.
    :type dst_img: np.ndarray
    :param src_pts: The initial keypoints/landmarks originating from the src image.
    :type src_pts: np.ndarray
    :param warped_src_pts: The warped keypoints/landmarks originating from the src image.
    :type warped_src_pts: np.ndarray
    :param dst_pts: The target keypoints/landmarks originating from the dst image.
    :type dst_pts: np.ndarray
    :param colors: The colors used to differentiate the different types of keypoints/landmarks.
    It accepts matplotlib colors, RGB or Hex values, defaults to ["green", "blue", "red"].
    :type colors: List[str], optional
    :param circle_size: The size of the circle indicating the location of the keypoints/landmarks.
    If ``auto`` will automatically compute the optimal size the circle should be drawn with,
    defaults to auto.
    :type circle_size: Union[str, int], optional
    :param fill_circle: Whether the circles should be filled or not, defaults to False.
    :type fill_circle: bool, optional
    :param figsize_max: The maximal figsize in inches, defaults to 20.0.
    :type figsize_max: float, optional
    :param legend: Whether to display the legend or not, defaults to True.
    :type legend: bool, optional
    :param legend_kwargs: The parameters of the legend, they can be composed of any kwargs of a
    matplotlib legend, defaults to {"fontsize": "x-large"}.
    :type legend_kwargs: Dict[str, Any], optional
    :raises ValueError: Provided ax is not a maplotlib.axes.Axes.
    :raises ValueError: Empty images or sets of keypoints/landmarks are not permitted.
    """

    if not isinstance(ax, Axes):
        raise ValueError(f"The axis provided is of type: {type(ax)}. Please provide instead a maplotlib.axes.Axes.")

    if not list(src_img) or not list(dst_img) or not list(src_pts) or not list(warped_src_pts) or not list(dst_pts):
        raise ValueError("Either one of the images or sets of points is empty. Please provide a valid input.")

    img_size = np.max((src_img.shape, dst_img.shape), axis=0)

    if circle_size == "auto":
        circle_size = compute_point_size_from_img_dim(img_size)

    fig = plt.gcf()
    fig.set_size_inches(*compute_optimal_fig_size(img_size, figsize_max))
    ax.set_axis_off()

    img_kwargs = {"alpha": 0.5}
    if img_size.shape[0] == 2:
        img_kwargs["cmap"] = "gray"

    ax.imshow(src_img, **img_kwargs)
    ax.imshow(dst_img, **img_kwargs)

    for src_pt, warped_src_pt, dst_pt in zip(src_pts, warped_src_pts, dst_pts):
        # Draw initial src keypoints/landmarks
        src_circle = plt.Circle((src_pt[0], src_pt[1]), circle_size, color=colors[0], fill=fill_circle)
        ax.add_artist(src_circle)

        # Draw warped src keypoints/landmarks
        warped_src_circle = plt.Circle(
            (warped_src_pt[0], warped_src_pt[1]), circle_size, color=colors[1], fill=fill_circle
        )
        ax.add_artist(warped_src_circle)

        # Draw target dst keypoints/landmarks
        dst_circle = plt.Circle((dst_pt[0], dst_pt[1]), circle_size, color=colors[2], fill=fill_circle)
        ax.add_artist(dst_circle)

        # Draw initial errors
        (initial_errors,) = ax.plot([src_pt[0], dst_pt[0]], [src_pt[1], dst_pt[1]], color=colors[0], linestyle="--")

        # Draw displacements
        (displacements,) = ax.plot(
            [src_pt[0], warped_src_pt[0]], [src_pt[1], warped_src_pt[1]], color=colors[1], linestyle="--"
        )

        # Draw remaining errors
        (remaining_errors,) = ax.plot(
            [warped_src_pt[0], dst_pt[0]], [warped_src_pt[1], dst_pt[1]], color=colors[2], linestyle="-"
        )

    if legend:
        ax.legend(
            [src_circle, warped_src_circle, dst_circle, initial_errors, displacements, remaining_errors],
            [
                "Initial src points",
                "Warped src points",
                "Target dst points",
                "Initial errors",
                "Displacements",
                "Remaining errors",
            ],
            **legend_kwargs,
        )


@with_logging(heading=get_heading(__name__, parent_folder=True))
def draw_matches(
    ax: Axes,
    src_img: np.ndarray,
    src_matched_kp: np.ndarray,
    dst_img: np.ndarray,
    dst_matched_kp: np.ndarray,
    good_indices: Optional[np.ndarray] = None,
    separation_gap: Optional[int] = None,
    cmap: colors.ListedColormap = cm.rainbow,
    circle_size: Union[str, int] = "auto",
    fill_circle: bool = False,
    figsize_max: float = 20.0,
) -> None:
    """Draw the matching keypoints between the src and dst images.

    :param ax: A matplotlib.axes.Axes used to display the drawn images and keypoints.
    :type ax: Axes
    :param src_img: The src image.
    :type src_img: np.ndarray
    :param src_matched_kp: The src matched keypoints.
    :type src_matched_kp: np.ndarray
    :param dst_img: The dst image.
    :type dst_img: np.ndarray
    :param dst_matched_kp: The dst matched keypoints.
    :type dst_matched_kp: np.ndarray
    :param good_indices: A boolean array the same length as the matched keypoints arrays
    indicating which the keypoints' correspondences should be displayed. If its value is
    ``None`` then all the correspondences are displayed, defaults to None.
    :type good_indices: Optional[np.ndarray], optional
    :param separation_gap: The size of the gap to insert between the images. If ``None``
    or ``0`` is provided, then no separation will occur, defaults to None.
    :type separation_gap: Optional[int], optional
    :param cmap: The colormap used to differentiate the matched keypoints, defaults to
    cm.rainbow.
    :type cmap: colors.ListedColormap, optional
    :param circle_size: The size of the circle indicating the location of the keypoints/landmarks.
    If ``auto`` will automatically compute the optimal size the circle should be drawn with,
    defaults to auto.
    :type circle_size: Union[str, int], optional
    :param fill_circle: Whether the circles should be filled or not, defaults to False.
    :type fill_circle: bool, optional
    :param figsize_max: The maximal figsize in inches, defaults to 20.0.
    :type figsize_max: float, optional
    :raises ValueError: Provided ax is not a maplotlib.axes.Axes.
    :raises ValueError: Empty images or sets of keypoints are not permitted.
    """

    if not isinstance(ax, Axes):
        raise ValueError(f"The axis provided is of type: {type(ax)}. Please provide instead a maplotlib.axes.Axes.")

    if not list(src_img) or not list(src_matched_kp) or not list(dst_img) or not list(dst_matched_kp):
        raise ValueError("Either one of the images or sets of keypoints is empty. Please provide a valid input.")

    to_stack = (src_img, dst_img)
    offset = src_img.shape[1]

    if separation_gap and separation_gap != 0:
        separation = np.ones((src_img.shape[0], separation_gap)) * 255
        to_stack = (src_img, separation, dst_img)
        offset += separation.shape[1]

    composite_img = np.hstack(to_stack)

    colors = cmap(np.linspace(0, 1, src_matched_kp.shape[0]))

    if circle_size == "auto":
        circle_size = compute_point_size_from_img_dim(composite_img.shape, composite=2)

    fig = plt.gcf()
    fig.set_size_inches(*compute_optimal_fig_size(composite_img.shape, figsize_max))
    ax.set_axis_off()

    ax.imshow(composite_img, cmap="gray")

    if good_indices is None or not list(good_indices):
        good_indices = np.ones((src_matched_kp.shape[0],), bool)

    for src_kp, dst_kp, line, color in zip(src_matched_kp, dst_matched_kp, good_indices, colors):
        # Draw src matched keypoints
        src_circle = plt.Circle((src_kp[0], src_kp[1]), circle_size, color=color, fill=fill_circle)
        ax.add_artist(src_circle)

        # Draw dst matched keypoints
        dst_circle = plt.Circle((offset + dst_kp[0], dst_kp[1]), circle_size, color=color, fill=fill_circle)
        ax.add_artist(dst_circle)

        # Draw lines for filtered keypoints
        if not line:
            continue

        ax.plot([src_kp[0], offset + dst_kp[0]], [src_kp[1], dst_kp[1]], color=color, linestyle="-")


def draw_boxplot(
    ax: Axes,
    distributions: List[ArrayLike],
    distrib_labels: List[str],
    xlabel: Optional[str] = None,
    ylabel: Optional[str] = None,
    log: bool = False,
    displayed_median_precision: str = "float_2",
    fontsize: int = 24,
    figsize_max: float = 20.0,
) -> None:
    """Draw the boxplot of the given distributions.

    :param ax: A matplotlib.axes.Axes used to display the drawn images and keypoints.
    :type ax: Axes
    :param distributions: The distributions that are plotted and compared.
    :type distributions: List[ArrayLike]
    :param distrib_labels: The labels of the given distributions.
    :type distrib_labels: List[str]
    :param xlabel: The xaxis label, defaults to None.
    :type xlabel: Optional[str], optional
    :param ylabel: The yaxis label, defaults to None.
    :type ylabel: Optional[str], optional
    :param log: Whether to plot the boxplot using a logarithmic scale for the yaxis, defaults to False.
    :type log: bool, optional
    :param displayed_median_precision: To which precision the median should be displayed in. It can either be
    ``int`` or ``float_x`` where x is the number of decimals, defaults to float_2.
    :type displayed_median_precision: int, optional
    :param fontsize: _description_, defaults to 12
    :type fontsize: int, optional
    :param figsize_max: The maximal figsize in inches, defaults to 20.0.
    :type figsize_max: float, optional
    :raises ValueError: Provided displayed_median_precision is not conform to the proposed options.
    """

    def __replace_units(label: str, rplcmnt: Optional[str] = "") -> str:
        if rplcmnt is None:
            return label

        if r := re.search(r"\[.*?\]", label):
            return label.replace(r.group(0), rplcmnt)

        return f"{label} {rplcmnt}"

    def __format_median(m: int | float, string_format: str) -> str:
        to_display = ""
        if string_format == "int":
            to_display = str(round(m))
        elif string_format == "float":
            to_display = str(round(m, 2))
        elif "float" in string_format:
            ndecimals = string_format.split("_")[-1]

            try:
                ndecimals = int(ndecimals)
            except ValueError:
                ndecimals = 2

            to_display = str(round(m, ndecimals))
        else:
            raise ValueError(
                f"Bad format ({string_format}), the possible values are: int, float, float_x (where x "
                "is the precision to which the median will be rounded)."
            )

        return to_display

    fig = plt.gcf()
    fig.set_size_inches((figsize_max, figsize_max))

    bp = ax.boxplot(distributions)

    rplcmnt = None
    if log:
        ax.set_yscale("log")
        rplcmnt = "(log)"

    # Write the values of each distribution's median
    medians = [item.get_ydata()[0] for item in bp["medians"]]
    for ind, m in enumerate(medians):
        ax.text(
            ind + 1, m, __format_median(m, displayed_median_precision), ha="center", va="bottom", fontsize=fontsize
        )

    ax.set_xticklabels(distrib_labels, rotation=45, fontsize=fontsize)
    ax.tick_params(axis="y", which="major", labelsize=fontsize)

    if xlabel and ylabel:
        ylabel = __replace_units(ylabel, rplcmnt)

        bigger_font = fontsize + fontsize // 4
        ax.set_xlabel(xlabel, fontsize=bigger_font)
        ax.set_ylabel(ylabel, fontsize=bigger_font)


def generate_standalone_figure(
    drawing_func: Callable[..., None],
    figsize_max: float = 20.0,
    out_img: Optional[str] = None,
    **drawing_func_kwargs: Any,
) -> Optional[np.ndarray]:
    """Generate a standalone matplotlib figure with given information to display.

    :param drawing_func: The function that will draw the wanted information.
    :type drawing_func: Callable[..., None]
    :param figsize_max: The maximal figsize in inches, defaults to 20.0.
    :type figsize_max: float, optional
    :param out_img: Either the path to the output image if one wants to save the results
    instead of displaying it or ``numpy`` which saves the figure in a np.ndarray or ``None``
    which will only display the results, defaults to None.
    :type out_img: Optional[str], optional
    :return: Either None or the image saved as a np.ndarray.
    :rtype: Optional[np.ndarray]

    Additionally, kwargs corresponding to the arguments of the given drawing function
    should be provided.
    """

    def _bytes_to_ndarray(_bytes: np.ndarray) -> np.ndarray:
        bytes_io = bytearray(_bytes)
        img = Image.open(io.BytesIO(bytes_io))

        return np.array(img)[..., :3]

    fig, ax = plt.subplots()

    drawing_func(ax=ax, figsize_max=figsize_max, **drawing_func_kwargs)

    if out_img == "numpy":
        buf = io.BytesIO()
        fig.savefig(buf, bbox_inches="tight", dpi="figure")
        buf.seek(0)

        img_arr = np.frombuffer(buf.getvalue(), dtype=np.uint8)
        img = _bytes_to_ndarray(img_arr)

        buf.close()
        plt.close()

        return img

    elif out_img:
        fig.subplots_adjust(left=0.0, right=1.0, top=1.0, bottom=0.0)

        logger.info(f"Exporting figure as {out_img}")

        # bbox_inches="tight" required to save the axes
        fig.savefig(out_img, bbox_inches="tight")

        plt.close()

        return

    plt.show()
