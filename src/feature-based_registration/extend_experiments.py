"""
Module used to extend each experiment's output with:
(i) The a mask to filter the landmarks common to all descriptor (missing landmarks in some cases due to the image edges)
(ii) The matching ranks of all dst landmarks
"""


import os
from collections import defaultdict
from typing import Callable, Dict, List, Tuple

import cv2
import numpy as np
import pandas as pd
from metrics.descriptive import compute_lndmrks_ranks

DESC_DIST_N_TYPE = {
    "Random": ["norm_l2", np.float32],
    "Neighborhood": ["norm_l2", np.float32],
    "SIFT": ["norm_l2", np.float32],
    "BRISK": ["norm_hamming", np.uint8],
    "FREAK": ["norm_hamming", np.uint8],
    "BRIEF": ["norm_hamming", np.uint8],
    "VGG": ["norm_l2", np.float32],
}

CV_DISTANCES = {
    "norm_l2": cv2.NORM_L2,
    "norm_l1": cv2.NORM_L1,
    "norm_hamming": cv2.NORM_HAMMING,
    "norm_hamming2": cv2.NORM_HAMMING2,
}


def _extract_coord_desc(df: pd.DataFrame) -> Tuple[np.ndarray]:
    """Extract the coordinates and descriptors.

    :param df: The dataframe from which the coordinates and the descriptors should be extracted.
    :type df: pd.DataFrame
    :return: The coordinates and the descriptors.
    :rtype: Tuple[np.ndarray]
    """

    coords = []
    desc = []
    for c in df.columns:
        if not c.startswith("desc"):
            if c.startswith("x") or c.startswith("y"):
                coords.append(df[c])
            continue

        desc.append(df[c])

    return np.array(coords).T, np.array(desc).T


def _multiply_masks(masks: List[np.ndarray]) -> np.ndarray:
    """Get a boolean mask that results from the combination of multiple masks.

    :param masks: The different masks to combine.
    :type masks: List[np.ndarray]
    :return mask: The resulting mask.
    :rtype: np.ndarray
    """

    mask = np.ones((masks[0].shape[0],), dtype=bool)
    for m in masks:
        mask *= m

    return mask


def _extract_src_lndmrks_coords(corresponding_exps: Tuple[str]) -> Dict[str, np.ndarray]:
    """Extract the src landmarks coordinates for all the experiments performed on the same image pair.

    :param corresponding_exps: The experiments paths.
    :type corresponding_exps: Tuple[str]
    :return descriptors_src_lndmrks_coords: The collection of src landmarks coordinates per experiment.
    (descriptor).
    :rtype: Dict[str, np.ndarray]
    """

    descriptors_src_lndmrks_coords = {}
    for exp in corresponding_exps:
        descriptor = os.path.basename(os.path.dirname(exp))
        src_df = pd.read_csv(os.path.join(exp, "supv/desc_src.csv"))
        descriptors_src_lndmrks_coords[descriptor] = np.array([src_df["x"], src_df["y"]]).T

    return descriptors_src_lndmrks_coords


def _compute_common_landmarks_masks(descriptors_src_lndmrks_coords: Dict[str, np.ndarray]) -> Dict[str, np.ndarray]:
    """Compute the mask of common landmarks throughout all the experiment concerning the same image pair.

    :param descriptors_src_lndmrks_coords: The collection of src landmarks coordinates per experiment.
    :type descriptors_src_lndmrks_coords: Dict[str, np.ndarray]
    :return masks: The collection of masks for each experiment.
    :rtype: Dict[str, np.ndarray]
    """

    masks = defaultdict(list)
    for key, value in descriptors_src_lndmrks_coords.items():
        for k, v in descriptors_src_lndmrks_coords.items():
            if key == k:
                masks[key].append(np.ones((value.shape[0],), dtype=bool))
            else:
                masks[key].append(_multiply_masks(list(np.in1d(value, v).reshape(value.shape).T)))
        masks[key] = _multiply_masks(masks[key])

    return masks


def _save_pair_lndmrks_mask(corresponding_exps: Tuple[str]) -> None:
    """Save the landmarks mask for each experiment that concerns a common image pair in the experiment folder at
    supv/lndmrks_mask.npy.

    :param corresponding_exps: The experiments paths.
    :type corresponding_exps: Tuple[str]
    """

    descriptors_src_lndmrks_coords = _extract_src_lndmrks_coords(corresponding_exps)
    masks = _compute_common_landmarks_masks(descriptors_src_lndmrks_coords)

    for exp in corresponding_exps:
        descriptor = os.path.basename(os.path.dirname(exp))
        np.save(os.path.join(exp, "supv/lndmrks_mask.npy"), masks[descriptor])


def _get_experiment_landmarks_matching_ranks(
    exp_path: str, distance: int = cv2.NORM_L2, dtype: type = np.float32
) -> np.ndarray:
    """Get the landmarks matching ranks when knn with all neighbors.

    :param exp_path: Path to the experiment.
    :type exp_path: str
    :param distance: The distance that is used to match the two sets of points,
    defaults to cv2.NORM_L2.
    :type distance: int, optional
    :param dtype: The descriptors' data type, defaults to np.float32.
    :type dtype: type, optional
    :return ranks: The ranks of the corresponding dst landmarks for the
    given experiment.
    :rtype: np.ndarray
    """

    _, src_lndmrks_desc = _extract_coord_desc(pd.read_csv(os.path.join(exp_path, "supv/desc_src.csv")))
    _, dst_lndmrks_desc = _extract_coord_desc(pd.read_csv(os.path.join(exp_path, "supv/desc_dst.csv")))

    mask = np.load(os.path.join(exp_path, "supv/lndmrks_mask.npy"))

    ranks = compute_lndmrks_ranks(
        src_lndmrks_desc[mask, :].astype(dtype), dst_lndmrks_desc[mask, :].astype(dtype), distance
    )

    # Correct the indices of the src landmarks with the mask exclusions
    ranks[:, 0] = np.arange(src_lndmrks_desc.shape[0])[mask]

    return ranks


def _save_exp_ranks(exp_path: str, ranks_method: Callable = _get_experiment_landmarks_matching_ranks) -> None:
    """Save the landmarks ranks in the experiment folder at supv/ranks_lndmrks_desc.csv.

    :param exp_path: The path to the experiment.
    :type exp_path: str
    :param ranks_method: The method used to compute the ranks, defaults to get_experiment_landmarks_match_ranks.
    :type ranks_method: Callable, optional
    """

    desc = os.path.basename(os.path.dirname(exp_path))

    d, t = DESC_DIST_N_TYPE[desc]
    ranks = ranks_method(exp_path, distance=CV_DISTANCES[d], dtype=t)

    df = pd.DataFrame(ranks, columns=["src_lndmrk_ind", "dst_lndmrks_matching_rank"])
    df.to_csv(os.path.join(exp_path, "supv/ranks_lndmrks_desc.csv"))


def generate_preproc_exp_list(preproc_path: str) -> Dict[str, List[str]]:
    """Generate the experiments lists, per descriptors, for a given pre-processing.

    :param preproc_path: The path to the given pre-processing folder.
    :type preproc_path: str
    :return preproc_dict: The collection of experiments lists per descriptors for the given pre-processing.
    :rtype: Dict[str, List[str]]
    """

    preproc_dict = defaultdict(list)

    for desc in os.listdir(preproc_path):
        if desc not in DESC_DIST_N_TYPE.keys():
            continue

        desc_path = os.path.join(preproc_path, desc)
        for exp in os.listdir(desc_path):
            if not exp.startswith("Feature"):
                continue

            exp_path = os.path.join(desc_path, exp)
            preproc_dict[desc].append(exp_path)

    return preproc_dict


def _generate_exp_zip(preproc_dict: Dict[str, List[str]]) -> zip:
    """Generate the n-uple of experiments related the same image pair for each image pair.

    :param preproc_dict: The collection of experiments lists per descriptors for the given pre-processing.
    :type preproc_dict: Dict[str, List[str]]
    :return: The list of n-uples of experiments related the same image pair,
    :rtype: zip
    """

    to_zip = [None] * len(preproc_dict.keys())
    for key, value in preproc_dict.items():
        to_zip[list(DESC_DIST_N_TYPE.keys()).index(key)] = sorted(value)

    return zip(*to_zip)


def extend_preproc_supv_outputs(preproc_path: str) -> None:
    """Extend each experiment supv output for a given pre-processing with the landmarks' mask and the matching
    ranks of dst landmarks.

    :param preproc_path: The path to the given pre-processing folder.
    :type preproc_path: str
    """

    preproc_dict = generate_preproc_exp_list(preproc_path)

    for exps in _generate_exp_zip(preproc_dict):
        _save_pair_lndmrks_mask(exps)
        for exp in exps:
            _save_exp_ranks(exp)
