import functools
import logging
from time import perf_counter
from typing import Any, Callable, Optional

logger = logging.getLogger("name")


def benchmark(func: Callable[..., Any]) -> Callable[..., Any]:
    """Decorator that benchmarks the execution time of a given function.

    :param func: The function to benchmark.
    :type func: Callable[..., Any]
    :return: The wrapper which will actually benchmark the function and return its outputs.
    :rtype: Callable[..., Any]
    """
    if "." in func.__qualname__:

        @functools.wraps(func)
        def wrapper(self, *args: Any, **kwargs: Any) -> Any:
            message = func.__qualname__
            if hasattr(self, "method"):
                method_name = message.split(".")[-1]
                message = message.replace(f".{method_name}", f"({self.method.__class__.__name__}).{method_name}")

            start_time = perf_counter()
            value = func(self, *args, **kwargs)
            end_time = perf_counter()
            run_time = end_time - start_time

            logger.info(f"{message}: execution time = {run_time:.2f} seconds.")
            self.exec_time = run_time

            return value

    else:

        @functools.wraps(func)
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            message = func.__qualname__

            start_time = perf_counter()
            value = func(*args, **kwargs)
            end_time = perf_counter()
            run_time = end_time - start_time

            logger.info(f"{message}: execution time = {run_time:.2f} seconds.")

            return value

    return wrapper


def with_logging(heading: Optional[str] = None):
    """Closure that allows to pass a additional information to the decorator.

    :param heading: Add a heading to the loggings, defaults to None.
    :type heading: Optional[str], optional
    """

    def decorator(func: Callable[..., Any]) -> Callable[..., Any]:
        """Decorator that add logging information to a given function.

        :param func: The function to log.
        :type func: Callable[..., Any]
        :return: The wrapper which will actually log the function.
        :rtype: Callable[..., Any]
        """

        if "." in func.__qualname__:

            @functools.wraps(func)
            def wrapper(self, *args: Any, **kwargs: Any) -> Any:
                if heading:
                    logger.info(heading)

                message = func.__qualname__
                if hasattr(self, "method"):
                    method_name = message.split(".")[-1]
                    message = message.replace(f".{method_name}", f"({self.method.__class__.__name__}).{method_name}")

                logger.info(f"Calling {message}")
                value = func(self, *args, **kwargs)
                logger.info(f"Finished {message}")

                return value

        else:

            @functools.wraps(func)
            def wrapper(*args: Any, **kwargs: Any) -> Any:
                message = func.__qualname__

                if heading:
                    logger.info(heading)

                logger.info(f"Calling {message}")
                value = func(*args, **kwargs)
                logger.info(f"Finished {message}")

                return value

        return wrapper

    return decorator
