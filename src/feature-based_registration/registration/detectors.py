from typing import Any, Callable, Optional

import cv2
import numpy as np
from profiling.decorators import benchmark, with_logging
from profiling.utils import get_heading
from registration.utils import available
from skimage import feature

AVAILABLE_DETECTORS = {
    "SIFT": cv2.SIFT_create,
    "SURF": cv2.xfeatures2d.SURF_create,
    "BRISK": cv2.BRISK_create,
    "ORB": cv2.ORB_create,
    "KAZE": cv2.KAZE_create,
    "AKAZE": cv2.AKAZE_create,
    "STAR": cv2.xfeatures2d.StarDetector_create,  # derived from CenSurE
}


class Detector:
    """Class to implement a features detector callable."""

    def __init__(self, method: Callable[..., Any], **kwargs: Any) -> None:
        """Constructor of Detector class.

        :param method: The method that will be used to detect the keypoints.
        :type method: Callable[..., Any]

        Additionally, kwargs specific to the method to use can be provided.
        """

        self.exec_time: Optional[float] = None

        self.method = method(**kwargs)
        self.method_kwargs = kwargs

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.method=}, {self.method_kwargs=})"

    @with_logging(heading=get_heading(__name__))
    @benchmark
    def __call__(self, img: np.ndarray) -> np.ndarray:
        """Detect the image keypoints.

        :param img: The image from which the keypoints are derived.
        :type img: np.ndarray
        :return keypoints: The detected keypoints.
        :rtype: np.ndarray
        """

        keypoints = self.method.detect(img)
        return keypoints


@available(AVAILABLE_DETECTORS)
class skCENSURE:
    """A CENSURE feature detector from scikit image.

    This scikit-image feature detecotr can be used as an OpenCV feature detector.
    """

    def __init__(self, **kwargs):
        """Constructor of skCENSURE class.

        Allowed args are similar to the ones from skimage.feature.CENSURE.
        """

        self.__detector = feature.CENSURE(**kwargs)

    def detect(self, img: np.ndarray):
        """Detect keypoints in image using CENSURE.

        See https://scikit-image.org/docs/dev/api/skimage.feature.html#skimage.feature.CENSURE.
        Uses keypoint info to create KeyPoint objects for OpenCV.

        :param img: Image from which keypoints will be detected.
        :type img: np.ndarray
        :return kp: List of OpenCV KeyPoint objects
        :rtype kp:
        """

        self.__detector.detect(img)

        # Transform (row, col) keypoints to (x, y) keypoints
        kp_xy = self.__detector.keypoints[:, ::-1].astype(float)
        kp = cv2.KeyPoint_convert(kp_xy.tolist())

        return kp
