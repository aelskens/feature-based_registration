from dataclasses import dataclass, field
from typing import Callable, Optional, Protocol, Tuple

import numpy as np
from registration.features_extraction import Features
from skimage.transform import ProjectiveTransform

MatchingMethod = Callable[[np.ndarray, np.ndarray], Tuple[np.ndarray, np.ndarray, np.ndarray]]
FilteringMethod = Callable[[np.ndarray, np.ndarray], Tuple[ProjectiveTransform, np.ndarray]]


class FilteredMatchedFeatures(Protocol):
    src_features: Features
    dst_features: Features
    match_method: MatchingMethod
    filter_method: FilteringMethod
    matched_src_keypoints_xy: np.ndarray
    matched_dst_keypoints_xy: np.ndarray
    good_matches_indices: np.ndarray
    filtered_matched_src_keypoints_xy: np.ndarray
    filtered_matched_dst_keypoints_xy: np.ndarray
    robust_transform: ProjectiveTransform


@dataclass
class ModularMatchingFiltering:
    """A class that matches and filters two sets of keypoints based on their feature vectors.

    :param src_features: The keypoints detected and described in the src image.
    :type src_features: Features
    :param dst_features: The keypoints detected and described in the dst image.
    :type dst_features: Features
    :param match_method: The method used for the matching of the two sets of keypoints based on
        their descriptions.
    :type match_method: MatchingMethod
    :param filter_method: The filtering method that removes outliers in the matched set of keypoints.
    :type filter_method: FilteringMethod
    :param filtered_matched_src_keypoints_xy: The filtered matching keypoints, as coordinates, from
        the src image.
    :type filtered_matched_src_keypoints_xy: np.ndarray
    :param filtered_matched_dst_keypoints_xy: The filtered matching keypoints, as coordinates, from
        the dst image.
    :type filtered_matched_dst_keypoints_xy: np.ndarray
    :param robust_transform: The robust transformation found based on the two sets of matching points.
    :type robust_transform: ProjectiveTransform
    """

    src_features: Features
    dst_features: Features
    match_method: MatchingMethod
    filter_method: FilteringMethod

    __matched_src_keypoints_xy: Optional[np.ndarray] = field(init=False, repr=False, default=None)
    __matched_dst_keypoints_xy: Optional[np.ndarray] = field(init=False, repr=False, default=None)
    __distances: Optional[np.ndarray] = field(init=False, repr=False, default=None)
    __robust_transform: Optional[ProjectiveTransform] = field(init=False, repr=False, default=None)
    __good_matches_indices: Optional[np.ndarray] = field(init=False, repr=False, default=None)
    __filtered_matched_src_keypoints_xy: Optional[np.ndarray] = field(init=False, repr=False, default=None)
    __filtered_matched_dst_keypoints_xy: Optional[np.ndarray] = field(init=False, repr=False, default=None)

    def __post_init__(self) -> None:
        src_matched_idx, dst_matched_idx, self.__distances = self.match_method(
            self.src_features.kp_descriptors, self.dst_features.kp_descriptors
        )

        self.__matched_src_keypoints_xy = self.src_features.keypoints[src_matched_idx, :]
        self.__matched_dst_keypoints_xy = self.dst_features.keypoints[dst_matched_idx, :]

        self.__robust_transform, self.__good_matches_indices = self.filter_method(
            self.__matched_src_keypoints_xy,
            self.__matched_dst_keypoints_xy,
            distances=self.__distances,
            src_img_shape=self.src_features.img.shape,
            dst_img_shape=self.dst_features.img.shape,
        )

        self.__filtered_matched_src_keypoints_xy = self.__matched_src_keypoints_xy[self.__good_matches_indices, :]
        self.__filtered_matched_dst_keypoints_xy = self.__matched_dst_keypoints_xy[self.__good_matches_indices, :]

    @property
    def matched_src_keypoints_xy(self) -> np.ndarray:
        return self.__matched_src_keypoints_xy

    @property
    def matched_dst_keypoints_xy(self) -> np.ndarray:
        return self.__matched_dst_keypoints_xy

    @property
    def good_matches_indices(self) -> np.ndarray:
        return self.__good_matches_indices

    @property
    def filtered_matched_src_keypoints_xy(self) -> np.ndarray:
        return self.__filtered_matched_src_keypoints_xy

    @property
    def filtered_matched_dst_keypoints_xy(self) -> np.ndarray:
        return self.__filtered_matched_dst_keypoints_xy

    @property
    def robust_transform(self) -> ProjectiveTransform:
        return self.__robust_transform
