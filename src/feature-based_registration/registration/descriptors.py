from typing import Any, Callable, Optional, Tuple

import cv2
import numpy as np
from openwholeslide import IntVector
from profiling.decorators import benchmark, with_logging
from profiling.utils import get_heading
from registration.utils import available
from skimage.morphology import square

AVAILABLE_DESCRIPTORS = {
    "SIFT": cv2.SIFT_create,
    # "SURF": cv2.xfeatures2d.SURF_create,
    "BRISK": cv2.BRISK_create,
    "ORB": cv2.ORB_create,
    "KAZE": cv2.KAZE_create,
    "AKAZE": cv2.AKAZE_create,
    "DAISY": cv2.xfeatures2d.DAISY_create,
    "LATCH": cv2.xfeatures2d.LATCH_create,
    "VGG": cv2.xfeatures2d.VGG_create,
    "Boost": cv2.xfeatures2d.BoostDesc_create,
    "BRIEF": cv2.xfeatures2d.BriefDescriptorExtractor_create,
    "FREAK": cv2.xfeatures2d.FREAK_create,
}


class Descriptor:
    """Class to implement a features descriptor callable."""

    def __init__(self, method: Callable[..., Any], **kwargs: Any) -> None:
        """Constructor of Descriptor class.

        :param method: The method that will be used to describe the keypoints.
        :type method: Callable[..., Any]

        Additionally, kwargs specific to the method to use can be provided.
        """

        self.exec_time: Optional[float] = None

        self.method = method(**kwargs)
        self.method_kwargs = kwargs

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.method=}, {self.method_kwargs=})"

    @with_logging(heading=get_heading(__name__))
    @benchmark
    def __call__(self, img: np.ndarray, keypoints: Tuple[cv2.KeyPoint]) -> Tuple[Tuple[cv2.KeyPoint], np.ndarray]:
        """Compute the feature vector for each keypoint.

        :param img: The image from which the keypoints are derived.
        :type img: np.ndarray
        :param keypoints: The keypoints to describe.
        :type keypoints: Tuple[cv2.KeyPoint]
        :return kp: The given keypoints.
        :rtype: Tuple[cv2.KeyPoint]
        :return kp_descriptors: The feature vectors describing the keypoints.
        :rtype: np.ndarray
        """

        kp, kp_descriptors = self.method.compute(img, keypoints)
        return kp, kp_descriptors


@available(AVAILABLE_DESCRIPTORS)
class Random:
    """Random descriptor used as a sanity check."""

    def __init__(self, dimensions: int = 64) -> None:
        """Random constructor.

        :param dimensions: The number of dimensions the descriptor should have, defaults to 32.
        :type dimensions: int, optional
        """

        self.dimensions = dimensions

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.dimensions=})"

    def compute(self, img: np.ndarray, keypoints: Tuple[cv2.KeyPoint]) -> Tuple[Tuple[cv2.KeyPoint], np.ndarray]:
        """Compute the random feature vector of a give dimension for each keypoint.

        :param img: The image from which the keypoints are derived.
        :type img: np.ndarray
        :param keypoints: The keypoints to describe.
        :type keypoints: Tuple[cv2.KeyPoint]
        :return kp: The given keypoints.
        :rtype: Tuple[cv2.KeyPoint]
        :return kp_descriptors: The feature vectors describing the keypoints.
        :rtype: np.ndarray
        """

        # Cast to float32 because OpenCV BF matcher can only take in uint8 or float32
        desc = np.random.rand(len(keypoints), self.dimensions).astype(np.float32)

        return keypoints, desc


@available(AVAILABLE_DESCRIPTORS)
class Neighborhood:
    """Neighborhood descriptor is a naive descriptor to use as a baseline."""

    def __init__(self, window_element: Callable[..., np.ndarray] = square, window_size: int = 3) -> None:
        """Neighborhood constructor.

        :param window_element: The window element that will be used to define the neighborhood. This element
        should be similar to skimage.morphology window element, defaults to skimage.morphology.square.
        :type window_element: Callable[..., np.ndarray], optional
        :param window_size: The size of the window use to get the neighborhood, it should be an odd number.
        If an even number is provided, then 1 is added, defaults to 3.
        :type window_size: int, optional
        """

        if window_size % 2 != 1:
            window_size += 1

        self.kernel = window_element(window_size, dtype=bool)
        self.kernel_size = self.kernel.shape[0]

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.kernel=}, {self.kernel_size=})"

    def __pad_img(self, img: np.ndarray) -> Tuple[np.ndarray]:
        pad_width = [(self.kernel_size, self.kernel_size)] * 2
        return self.kernel_size, np.pad(img, pad_width, constant_values=255)

    def __get_neighborhood(self, img: np.ndarray, coords: IntVector) -> np.ndarray:
        squared_neighborhood = img[
            coords.y - self.kernel_size // 2 : coords.y + self.kernel_size // 2 + 1,
            coords.x - self.kernel_size // 2 : coords.x + self.kernel_size // 2 + 1,
        ]
        neighborhood = squared_neighborhood[self.kernel]

        return neighborhood

    def compute(self, img: np.ndarray, keypoints: Tuple[cv2.KeyPoint]) -> Tuple[Tuple[cv2.KeyPoint], np.ndarray]:
        """Compute the feature vector for each keypoint based on its neighborhood.

        :param img: The image from which the keypoints are derived.
        :type img: np.ndarray
        :param keypoints: The keypoints to describe.
        :type keypoints: Tuple[cv2.KeyPoint]
        :return kp: The given keypoints.
        :rtype: Tuple[cv2.KeyPoint]
        :return kp_descriptors: The feature vectors describing the keypoints.
        :rtype: np.ndarray
        """

        # Pad the image to avoid issues at the image's borders
        offset, padded_img = self.__pad_img(img)

        desc = []

        for kp in [elem.pt for elem in keypoints]:
            coords = IntVector.from_xy(np.array([round(i) for i in kp]) + offset)
            desc.append(self.__get_neighborhood(padded_img, coords))

        return keypoints, np.array(desc)
