from dataclasses import dataclass, field
from typing import Callable, Optional, Protocol, Tuple

import cv2
import numpy as np
from registration.descriptors import AVAILABLE_DESCRIPTORS, Descriptor
from registration.detectors import AVAILABLE_DETECTORS, Detector

# Maximum number of image features that will be recorded. If the number
# of features exceeds this value, the MAX_FEATURES features with the
# highest response will be returned.
MAX_FEATURES = 20000

FeatureDetector = Callable[[np.ndarray], np.ndarray]
FeatureDescriptor = Callable[[np.ndarray, Tuple[cv2.KeyPoint]], Tuple[Tuple[cv2.KeyPoint], np.ndarray]]


class Features(Protocol):
    img: np.ndarray
    detector: FeatureDetector
    descriptor: FeatureDescriptor
    keypoints: np.ndarray
    kp_descriptors: np.ndarray


@dataclass
class ModularDetectionDescription:
    """A class that stores the features detected and described in a given whole slide image with
    given detection and description methods.

    :param img: The given image provided as a ndarray.
    :type img: np.ndarray
    :param detector: The technique used to detect the keypoints. Any callable that detects and
        returns the found keypoints based on an image, defaults to riem.registration.detectors.BRISK.
    :type detector: FeatureDetector
    :param descriptor: The descriptor technique used to extract the feature vector for each keypoints.
        Any callable that implements a descriptor algorithm based on a given image and its detected keypoints
        and which returns the keypoints' features vectors, defaults to riem.registration.descriptors.VGG.
    :type descriptor: FeatureDescriptor
    :param keypoints: The keypoints detected, as coordinates, in the given image using the given FeatureDetector
        method, if m points where detected the shape of keypoints is m x 2.
    :type keypoints: np.ndarray
    :param kp_descriptors: The descriptors of each detected keypoint obtained with the given FeatureDescriptor
        method, if m points where detected and n descriptors are used to describe them then the shape of
        kp_descriptors is m x n.
    :type kp_descriptors: np.ndarray
    """

    img: np.ndarray
    detector: FeatureDetector = Detector(method=AVAILABLE_DETECTORS["BRISK"])
    descriptor: FeatureDescriptor = Descriptor(method=AVAILABLE_DESCRIPTORS["VGG"])

    __keypoints: Optional[np.ndarray] = field(init=False, repr=False, default=None)
    __kp_descriptors: Optional[np.ndarray] = field(init=False, repr=False, default=None)

    def __post_init__(self) -> None:
        tmp_keypoints = self.detector(self.img)
        tmp_keypoints = self.remove_excessive_kp(tmp_keypoints)
        keypoints, self.__kp_descriptors = self.descriptor(self.img, tmp_keypoints)
        self.__keypoints = np.array([kp.pt for kp in keypoints])

    def remove_excessive_kp(self, keypoints: Tuple[cv2.KeyPoint], nb_max: int = MAX_FEATURES) -> Tuple[cv2.KeyPoint]:
        """Remove excessive keypoint.

        :param keypoints: The detected keypoints.
        :type keypoints: Tuple[cv2.KeyPoint]
        :param nb_max: The maximal number of keypoints that are taken into account, defaults to MAX_FEATURES
        :type nb_max: int, optional
        :return: The nb_max best keypoints.
        :rtype: Tuple[cv2.KeyPoint]
        """

        response = np.array([x.response for x in keypoints])
        keep_idx = np.argsort(response)[::-1][:nb_max]

        return tuple(keypoints[i] for i in keep_idx)

    @property
    def keypoints(self) -> np.ndarray:
        return self.__keypoints

    @property
    def kp_descriptors(self) -> np.ndarray:
        return self.__kp_descriptors
