import importlib
from typing import Any, Callable, Dict

CLS_MODULE = {
    "ValisExtended": "riem.registration.wsi",
    "Luminosity": "valis.preprocessing",
    "BioFormatsSlideReader": "valis.slide_io",
}


def get_cls_from_str(module_file: str, cls: str) -> Callable[..., Any]:
    """Dynamically import and extract the desired class.

    :param module_file: Module to import
    :type module_file: str
    :param cls: Class to import and extract
    :type cls: str
    :return: The desired class
    :rtype: Callable[..., Any]
    """

    return getattr(importlib.import_module(module_file), cls)


def available(methods: Dict[str, Callable[..., Any]]) -> Callable[..., Any]:
    """Closure that allows to provide the methods dictionary to the decorator.

    :param methods: The methods dictionary.
    :type methods: Dict[str, Callable[..., Any]]
    """

    def decorator(cls: Callable[..., Any]) -> Callable[..., Any]:
        """Add the given class in the methods dictionary.

        :param cls: The class to add in the methods dictionary.
        :type cls: Callable[..., Any]
        :return: The class itself.
        :rtype: Callable[..., Any]
        """

        methods[cls.__name__] = cls
        return cls

    return decorator
