from typing import Any, Optional, Tuple

import cv2
import numpy as np
from profiling.decorators import benchmark, with_logging
from profiling.utils import get_heading
from sklearn.metrics import pairwise_distances


class PairwiseDistances:
    """Class that implements the matching of two sets of descriptors based on a distance
    metric.
    """

    def __init__(self, **kwargs: Any) -> None:
        """PairwiseDistance constructor.

        The implementation is based on sklearn pairwise_distance function. Therefore, the
        constructor of this class accepts the different parameters of the later function
        as kwargs.

        The following kwargs are prohibited: ``X``, ``Y``, ``src`` and ``dst``, those are
        removed if present.
        """

        self.exec_time: Optional[float] = None

        self.__matcher = pairwise_distances

        for k in kwargs.copy():
            if k not in ["X", "Y", "src", "dst"]:
                continue

            del kwargs[k]

        self.__matcher_kwargs = kwargs

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.__matcher=}, {self.__matcher_kwargs=})"

    @with_logging(heading=get_heading(__name__))
    @benchmark
    def __call__(self, src: np.ndarray, dst: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Retrieve the indices of the matched keypoints based on their descriptors.

        :param src: Source descriptors.
        :type src: np.ndarray
        :param dst: Destination descriptors.
        :type dst: np.ndarray
        :return src_indices: The indices of the source matched keypoints.
        :rtype: np.ndarray
        :return dst_indices: The indices of the destination matched keypoints.
        :rtype: np.ndarray
        :return distances: The distances between the pairs of matched keypoints.
        :rtype: np.ndarray
        """

        distances = self.__matcher(src, dst, **self.__matcher_kwargs)

        src_indices = np.arange(src.shape[0])
        dst_indices = np.argmin(distances, axis=1)

        return src_indices, dst_indices, distances[src_indices, dst_indices]


class BruteForce:
    """Class that implements a brute force matching method of two sets of descriptors based
    on a distance metric.
    """

    def __init__(self, distance: str = "norm_l2", cross_check: bool = False) -> None:
        """BruteForce constructor.

        :param distance: The distance to use for the matching part, defaults to ``norm_l2``.
        :type distance: str, optional
        :param cross_check: Define whether or not a cross check to remove the single direction
        matches (the i-th descriptor in set A has the j-th descriptor in set B as the best match
        while the inverse is not true) should used, defaults to ``False``.
        :type cross_check: bool, optional
        """

        self.exec_time: Optional[float] = None

        CV_DISTANCES = {
            "norm_l2": cv2.NORM_L2,
            "norm_l1": cv2.NORM_L1,
            "norm_hamming": cv2.NORM_HAMMING,
            "norm_hamming2": cv2.NORM_HAMMING2,
        }

        d = CV_DISTANCES.get(distance, None)
        if not d:
            raise KeyError(
                f"{distance} is not one of the existing options OpenCV BFMatcher. The available "
                f"options are the following {','.join(CV_DISTANCES.keys())}."
            )

        self.__matcher = cv2.BFMatcher(d, crossCheck=cross_check)
        self.__matcher_kwargs = {"distance": distance, "cross_check": cross_check}

    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}({self.__matcher=}, {self.__matcher_kwargs=})"

    @with_logging(heading=get_heading(__name__))
    @benchmark
    def __call__(self, src: np.ndarray, dst: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Retrieve the indices of the matched keypoints based on their descriptors.

        :param src: Source descriptors.
        :type src: np.ndarray
        :param dst: Destination descriptors.
        :type dst: np.ndarray
        :return src_indices: The indices of the source matched keypoints.
        :rtype: np.ndarray
        :return dst_indices: The indices of the destination matched keypoints.
        :rtype: np.ndarray
        :return distances: The distances between the pairs of matched keypoints.
        :rtype: np.ndarray
        """

        matches = self.__matcher.match(src, dst)

        indices = np.array([[match.queryIdx, match.trainIdx, match.distance] for match in matches])

        src_indices = indices[:, 0].astype(int)
        dst_indices = indices[:, 1].astype(int)
        distances = indices[:, 2]

        return src_indices, dst_indices, distances
