import json
import logging
import os
from dataclasses import dataclass, field
from datetime import datetime
from types import NoneType
from typing import Any, Callable, Dict, List, Optional, Tuple, Union

import cv2
import numpy as np
import pandas as pd
import pytz
from errors import PathNotADirectoryError
from matplotlib import colors
from metrics.registration import rmse, tre
from openwholeslide import (
    FloatVector,
    IntVector,
    PaddingParameters,
    SlideRegion,
    VectorType,
    WholeSlide,
)
from processing.transform import Warping2D
from psutil import Process
from registration.features_extraction import Features
from registration.features_matching import FilteredMatchedFeatures
from skimage.io import imsave
from viz.colormaps import generate_jzazbz_cmap
from viz.wsi import (
    draw_init_warped_target,
    draw_matches,
    draw_overlap_img,
    generate_standalone_figure,
)

PreProcessing = Callable[..., Union[np.ndarray, Tuple[np.ndarray]]]

ALL_OUTPUTS = [
    "LOGS",
    "LOGFILE",
    "INIT",
    "PREPROC",
    "FEATURES",
    "MATCHED",
    "FILTERED",
    "UNSUPV_QUANT",
    "UNSUPV_QUAL",
    "SUPV_QUANT",
    "SUPV_QUAL",
    "OVERLAP",
]


@dataclass
class FeatureBasedExperiment:
    """A class that allows to run modular experiments for feature-based registration."""

    src_wsi: WholeSlide
    dst_wsi: WholeSlide

    outpath: Optional[str] = None
    output_level: List[str] = field(default_factory=list)

    magnification: float = 1.0
    src_gt_path: Optional[str] = None
    dst_gt_path: Optional[str] = None

    preprocessing_cls: Optional[PreProcessing] = field(repr=False, default=None)
    preprocessing_kwargs: Optional[Dict[str, Any]] = None
    normalize_cls: Optional[PreProcessing] = field(repr=False, default=None)
    normalize_kwargs: Optional[Dict[str, Any]] = None
    features_cls: Optional[Features] = field(repr=False, default=None)
    features_kwargs: Optional[Dict[str, Any]] = None
    matching_cls: Optional[FilteredMatchedFeatures] = field(repr=False, default=None)
    matching_kwargs: Optional[Dict[str, Any]] = None

    __dir_path: Optional[str] = field(init=False, default=None)
    __logger: Optional[logging.Logger] = field(init=False, repr=False, default=logging.getLogger())

    __src_region: Optional[SlideRegion] = field(init=False, repr=False, default=None)
    __dst_region: Optional[SlideRegion] = field(init=False, repr=False, default=None)
    __src_pre_process: Optional[np.ndarray] = field(init=False, repr=False, default=None)
    __dst_pre_process: Optional[np.ndarray] = field(init=False, repr=False, default=None)
    __src_pad: Optional[PaddingParameters] = field(init=False, repr=False, default=None)
    __dst_pad: Optional[PaddingParameters] = field(init=False, repr=False, default=None)
    __src_features: Optional[Features] = field(init=False, repr=False, default=None)
    __dst_features: Optional[Features] = field(init=False, repr=False, default=None)
    __robust_estimation: Optional[FilteredMatchedFeatures] = field(init=False, repr=False, default=None)
    __warp2D: Optional[Warping2D] = field(init=False, repr=False, default=None)

    __exec_times: Dict[str, float] = field(init=False, repr=False, default_factory=dict)
    __eval_results: Optional[Dict[str, Any]] = field(init=False, repr=False, default=None)

    def __post_init__(self) -> None:
        # Clear handlers as if creating a new logger for each instance
        self.__logger.handlers.clear()

        if "LOGS" in self.output_level:
            self.set_logger_handler(self.get_add_handler("stdout", logging.StreamHandler()))

        self.set_experiment_dir(self.outpath)
        self.set_output_level(output_level=self.output_level)

        if "LOGFILE" in self.output_level:
            _handler = self.__generate_outputs(
                level_dir="logs",
                prefix="logs",
                suffix="general.log",
                save_method=logging.FileHandler,
                save_method_kwargs={},
                outpath_key="filename",
            )

            self.set_logger_handler(self.get_add_handler("file", _handler))

            # Save the experiment configuration in a config file
            self.__generate_outputs(
                level_dir="logs",
                prefix="logs",
                suffix="config.json",
                save_method=self.__save_config,
                save_method_kwargs={},
            )

    @property
    def dir_path(self) -> Optional[str]:
        return self.__dir_path

    def set_experiment_dir(self, outpath: Optional[str] = None, timezone: str = "CET") -> None:
        """Generate the experiment directory that will contain the various outputs.

        :param outpath: The location where the experiment directory should be generated. If ``None``
        provided then the directory will not be generated, defaults to None.
        :type outpath: Optional[str], optional
        :param timezone: The timezone from which the date and time will be retrieved
        for the name of the directory, defaults to "CET".
        :type timezone: str, optional
        :raises PathNotADirectoryError: The given outpath is not a directory.
        """

        self.__logger.info("Set experiment dir.")
        if not outpath:
            self.__dir_path = None
            return

        if not os.path.exists(outpath):
            os.mkdir(outpath)

        if not os.path.isdir(outpath):
            raise PathNotADirectoryError(path=outpath)

        tz = pytz.timezone(timezone)
        time = datetime.now(tz).strftime("%Y%m%d_%H%M%S")
        cpu_id = Process().cpu_num()
        dir_name = f"{self.__class__.__name__}_{time}_{cpu_id:02}"

        self.__dir_path = os.path.join(outpath, dir_name)
        os.mkdir(self.__dir_path)

    def set_output_level(self, output_level: List[str] = ["ALL"]) -> None:
        """Set the output level for the experiment.

        :param output_level: A list of strings that specifies which outputs should be saved.
        Each possibility is associated to a specific word such as ``'INIT'`` to save
        the initial images. Different combinations of outputs can be achieved by providing
        each desired output word, for instance: ``['INIT', 'PREPROC']`` will save the initial
        images and their pre-processed versions, defaults to 'ALL'. It is also possible to
        specify the undesired outputs rather than the desired ones by adding ``'NOT_'`` before
        the word, for example: ``'NOT_INIT'`` will save every possible outputs apart from
        the initial images. ``'NOT_ALL'`` will be interpreted as no outputs desired.
        All the possibilities are listed here:
        * ``'ALL'``: every possible outputs (when provided overrule anything else except
        when ``'NOT_'`` elements are also present)
        * ``'LOGS'``: log of all the processes via stdout
        * ``'LOGFILE'``: log of all the processes via a logfile
        * ``'INIT'``: initial images
        * ``'PREPROC'``: pre-processed images
        * ``'FEATURES'``: keypoints coordinates and descriptors
        * ``'MATCHED'``: matched keypoints coordinates
        * ``'FILTERED'``: filtered keypoints coordinates
        * ``'UNSUPV_QUANT'``: keypoints quantitative performance
        * ``'UNSUPV_QUAL'``: keypoints qualitative performance
        * ``'SUPV_QUANT'``: landmarks quantitative performance
        * ``'SUPV_QUAL'``: landmarks qualitative performance
        * ``'OVERLAP'``: overlapping of the initial and registered images
        :type output_level: Optional[str], optional
        """

        if not self.outpath:
            self.__logger.info("No experiment output directory. Output level set to [].")
            self.output_level = []
            return

        if not output_level:
            self.__logger.info(f"Due to provided value for {output_level=}, it is set to [].")
            self.output_level = []
            return

        def _split_desired_undesired(given: List[str]) -> Tuple[List[str]]:
            desired, undesired = [], []
            for g in given:
                if "NOT_" in g:
                    undesired.append(g.replace("NOT_", ""))
                else:
                    desired.append(g)
            return desired, undesired

        outputs = ALL_OUTPUTS.copy()

        desired, undesired = _split_desired_undesired(output_level)

        if "ALL" in undesired and "ALL" not in desired:
            self.output_level = []
        elif "ALL" in undesired and "ALL" in desired:
            self.__logger.info(
                "Inconsistancy, ALL and NOT_ALL have been specified. \n Resolved by taking only ALL into account."
            )
            self.output_level = outputs
        elif ("ALL" in desired) or (not desired and undesired):
            for n in undesired:
                outputs.remove(n)
            self.output_level = outputs
        elif desired and undesired:
            self.__logger.info(
                "Inconsistancy, presence of both desired and undesired outputs. \n"
                "Resolved by taking only the desired ones into account."
            )
            self.output_level = desired
        else:
            self.output_level = desired

    def __generate_subdir_if_missing(self, path: str) -> None:
        if not os.path.exists(path):
            os.mkdir(path)

    def __generate_outputs(
        self,
        level_dir: str,
        prefix: str,
        suffix: str,
        save_method: Callable[..., None],
        save_method_kwargs: Dict[str, Any],
        outpath_key: str = "outpath",
    ) -> Any:
        experiment_dir = self.__dir_path
        self.__generate_subdir_if_missing(os.path.join(experiment_dir, level_dir))

        outpath = os.path.join(experiment_dir, level_dir + os.sep + prefix + "_" + suffix)
        save_method_kwargs[outpath_key] = outpath

        return save_method(**save_method_kwargs)

    def __serialize_iterable(self, to_serialize: Union[Dict[str, Any], List[Any]]) -> Union[Dict[str, Any], List[Any]]:
        if isinstance(to_serialize, list):
            _iter = enumerate(to_serialize)
        elif isinstance(to_serialize, dict):
            _iter = to_serialize.items()

        for key, value in _iter:
            if isinstance(value, list):
                to_serialize[key] = self.__serialize_iterable(value.copy())
            elif isinstance(value, dict):
                to_serialize[key] = self.__serialize_iterable(value.copy())
            elif (
                not isinstance(value, str)
                and not isinstance(value, int)
                and not isinstance(value, float)
                and not isinstance(value, NoneType)
                and not isinstance(value, bool)
            ):
                to_serialize[key] = str(value)

        return to_serialize

    def __save_config(self, outpath: str) -> None:
        with open(outpath, "w") as file:
            json.dump(self.__serialize_iterable(self.__dict__.copy()), file)

    def __save_img(self, outpath: str, img: np.ndarray) -> None:
        imsave(outpath, img)

    def __save_points(self, outpath: str, coords: np.ndarray, descriptors: Optional[np.ndarray] = None) -> None:
        data = coords
        cols = ["x", "y"]

        if descriptors is not None:
            data = np.hstack([coords, descriptors])
            cols += [f"desc_{i+1}" for i in range(descriptors.shape[1])]

        df = pd.DataFrame(data, columns=cols)
        df.to_csv(outpath)

    def __save_exec_time(self, outpath: str, times: Dict[str, float]) -> None:
        df = pd.DataFrame([times])
        df.to_csv(outpath)

    def __add_suffix(self, fname: str, suffix: str) -> str:
        root, ext = os.path.splitext(fname)

        return root + "_" + suffix + ext

    def __save_metrics(self, outpath: str, quant: Dict[str, Any], gt_path: Optional[Dict[str, str]] = None) -> None:
        nb_common = min(len(pts) for pts in [quant["dst"], quant["src"]])

        cols = ["dst_path", "src_path"]
        pts_pairs = [[self.dst_wsi.path for i in range(nb_common)], [self.src_wsi.path for i in range(nb_common)]]

        _globals = {"dst_path": self.dst_wsi.path, "src_path": self.src_wsi.path}

        if gt_path:
            cols += list(gt_path.keys())
            pts_pairs += [
                [gt_path["dst_gt_path"] for i in range(nb_common)],
                [gt_path["src_gt_path"] for i in range(nb_common)],
            ]
            _globals.update(gt_path)

        cols += ["dst_coord_xy", "src_coord_xy", "warped_src_coord_xy"]
        pts_pairs += [
            quant["dst"][:nb_common, :],
            quant["src"][:nb_common, :],
            quant["warped"][:nb_common, :],
        ]

        for key, value in quant["metrics"]["local"].items():
            cols.append(key)
            pts_pairs.append(value)

            _mean = np.mean(value)
            _median = np.median(value)
            _lower_quartile = np.percentile(value, 25)
            _higher_quartile = np.percentile(value, 75)
            _std = np.std(value)

            _globals[f"{key}_mean"] = _mean
            _globals[f"{key}_median"] = _median
            _globals[f"{key}_lower_quartile"] = _lower_quartile
            _globals[f"{key}_higher_quartile"] = _higher_quartile
            _globals[f"{key}_std"] = _std

        df = pd.DataFrame(list(zip(*pts_pairs)), columns=cols)
        df.to_csv(self.__add_suffix(outpath, "local"))

        _globals.update(quant["metrics"]["global"])
        df = pd.DataFrame([_globals])
        df.to_csv(self.__add_suffix(outpath, "global"))

    def __get_logger_handler(self, handler_name: str) -> Optional[logging.Handler]:
        _handler = None

        if self.__logger.handlers:
            for h in self.__logger.handlers:
                if h.get_name() != handler_name:
                    continue

                _handler = h

        return _handler

    def __add_logger_handler(self, handler_name: str, _handler: logging.Handler) -> logging.Handler:
        _handler.set_name(handler_name)
        self.__logger.addHandler(_handler)

        return _handler

    def get_add_handler(self, handler_name: str, new_handler: Optional[logging.Handler] = None) -> logging.Handler:
        """Get or add (if necessary) a given logging handler.

        :param handler_name: The name that should be given to the handler.
        :type handler_name: str
        :param new_handler: The given handler to add. If there already exists a handler with the given name, this
        argument is overlooked. Otherwise, create a new handler with this argument, defaults to None.
        :type new_handler: Optional[logging.Handler], optional
        :raises TypeError: Triggered when there is no existing handler with the specified handler_name and the new_handler's type is not logging.Handler
        :return: The handler named as specified, either the existing one or the new one.
        :rtype: logging.Handler
        """

        _handler = self.__get_logger_handler(handler_name)

        if not _handler:
            if not isinstance(new_handler, logging.Handler):
                raise TypeError(
                    f"No existing handler named: {handler_name} and new handler of wrong type has been provided {type(new_handler)=}."
                )

            _handler = self.__add_logger_handler(handler_name, new_handler)

        return _handler

    def set_logger_handler(
        self,
        _handler: logging.Handler,
        level: int = logging.INFO,
        log_format: str = "[%(asctime)s] %(levelname)s: %(message)s",
    ) -> None:
        """Set an existing logger's handler to profile the execution of the experiment.

        :param level: The logging level which should be used, defaults to logging.INFO.
        :type level: int, optional
        :param log_format: The format of the logging messages, defaults to
        "[%(asctime)s] %(levelname)s: %(message)s".
        :type log_format: str, optional
        """

        self.__logger.setLevel(level)

        _handler.setLevel(level)
        _handler.setFormatter(logging.Formatter(log_format))

    def __load_image(self, ws: WholeSlide) -> SlideRegion:
        return ws.read_full(self.magnification)

    def __pad(self, img: np.ndarray, padding: np.ndarray) -> Tuple[np.ndarray, PaddingParameters]:
        pad_value = 0.0 if img.dtype != np.uint8 else 0

        pad_dimensions = IntVector.from_xy(padding) - IntVector.from_yx(img.shape[:2])
        pad_before = IntVector(x=pad_dimensions.x // 2, y=pad_dimensions.y // 2)

        padder = PaddingParameters(
            pad_before=pad_before,
            pad_after=IntVector(x=pad_dimensions.x - pad_before.x, y=pad_dimensions.y - pad_before.y),
            pad_value=pad_value,
        )

        return np.pad(img, padder.pad_width[:2], constant_values=padder.pad_value), padder

    def __preprocess(self, img: np.ndarray) -> np.ndarray:
        return self.preprocessing_cls(img, **self.preprocessing_kwargs)

    def __normalize(self, src: np.ndarray, dst: np.ndarray) -> Tuple[np.ndarray]:
        masking_func = self.normalize_kwargs.get("masking_func", None)
        masking_func_kwargs = self.normalize_kwargs.get("masking_func_kwargs", {})

        if masking_func:
            self.normalize_kwargs["src_mask"] = masking_func(src, **masking_func_kwargs)
            self.normalize_kwargs["dst_mask"] = masking_func(dst, **masking_func_kwargs)
            del self.normalize_kwargs["masking_func"]
            if masking_func_kwargs:
                del self.normalize_kwargs["masking_func_kwargs"]

        return self.normalize_cls(src, dst, **self.normalize_kwargs)

    def __extract_features(self, img: np.ndarray) -> Features:
        return self.features_cls(img, **self.features_kwargs)

    def __match_filter_estimate(self) -> FilteredMatchedFeatures:
        return self.matching_cls(self.__src_features, self.__dst_features, **self.matching_kwargs)

    def execute(self) -> None:
        """Execute the experiment with the given config. If the output level has been set, then
        outputs are generated accordingly.
        """

        self.__logger.info(f"src path: {self.src_wsi.path}")
        self.__logger.info(f"dst path: {self.dst_wsi.path}")

        # Load the region from the WSI
        self.__src_region = self.__load_image(self.src_wsi)
        self.__dst_region = self.__load_image(self.dst_wsi)

        # INIT outputs
        if "INIT" in self.output_level:
            self.__generate_outputs(
                level_dir="init",
                prefix="init",
                suffix="src.png",
                save_method=self.__save_img,
                save_method_kwargs={"img": self.__src_region.as_ndarray},
            )
            self.__generate_outputs(
                level_dir="init",
                prefix="init",
                suffix="dst.png",
                save_method=self.__save_img,
                save_method_kwargs={"img": self.__dst_region.as_ndarray},
            )

        # Preprocess the WSI
        src_pre_process = self.__preprocess(self.__src_region.as_ndarray)
        dst_pre_process = self.__preprocess(self.__dst_region.as_ndarray)

        # Compute and add padding (after pre-processing to avoid influencing it)
        init_src_dimensions, init_dst_dimensions = (
            self.__src_region.px_dimensions.xy,
            self.__dst_region.px_dimensions.xy,
        )
        pad_dimensions_xy = np.max((init_src_dimensions, init_dst_dimensions), axis=0)
        self.__src_pre_process, self.__src_pad = self.__pad(src_pre_process, pad_dimensions_xy)
        self.__dst_pre_process, self.__dst_pad = self.__pad(dst_pre_process, pad_dimensions_xy)

        if self.normalize_cls:
            self.__src_pre_process, self.__dst_pre_process = self.__normalize(
                self.__src_pre_process, self.__dst_pre_process
            )

        # PREPROC outputs
        if "PREPROC" in self.output_level:
            self.__generate_outputs(
                level_dir="preproc",
                prefix="preproc",
                suffix="src.png",
                save_method=self.__save_img,
                save_method_kwargs={"img": self.__src_pre_process},
            )
            self.__generate_outputs(
                level_dir="preproc",
                prefix="preproc",
                suffix="dst.png",
                save_method=self.__save_img,
                save_method_kwargs={"img": self.__dst_pre_process},
            )

        # Extract the features and store execution time
        self.__src_features = self.__extract_features(self.__src_pre_process)
        self.__exec_times["src_detector"] = self.__src_features.detector.exec_time
        self.__exec_times["src_descriptor"] = self.__src_features.descriptor.exec_time
        self.__dst_features = self.__extract_features(self.__dst_pre_process)
        self.__exec_times["dst_detector"] = self.__dst_features.detector.exec_time
        self.__exec_times["dst_descriptor"] = self.__dst_features.descriptor.exec_time

        # FEATURES outputs and store execution time
        if "FEATURES" in self.output_level:
            self.__generate_outputs(
                level_dir="features",
                prefix="raw",
                suffix="src.csv",
                save_method=self.__save_points,
                save_method_kwargs={
                    "coords": self.__src_features.keypoints,
                    "descriptors": self.__src_features.kp_descriptors,
                },
            )
            self.__generate_outputs(
                level_dir="features",
                prefix="raw",
                suffix="dst.csv",
                save_method=self.__save_points,
                save_method_kwargs={
                    "coords": self.__dst_features.keypoints,
                    "descriptors": self.__dst_features.kp_descriptors,
                },
            )

        # Match, filter and estimate and store execution time
        self.__robust_estimation = self.__match_filter_estimate()
        self.__exec_times["matching"] = self.__robust_estimation.match_method.exec_time
        self.__exec_times["filtering"] = self.__robust_estimation.filter_method.exec_time

        # MATCHED outputs
        if "MATCHED" in self.output_level:
            self.__generate_outputs(
                level_dir="features",
                prefix="matched",
                suffix="src.csv",
                save_method=self.__save_points,
                save_method_kwargs={
                    "coords": self.__robust_estimation.matched_src_keypoints_xy,
                },
            )
            self.__generate_outputs(
                level_dir="features",
                prefix="matched",
                suffix="dst.csv",
                save_method=self.__save_points,
                save_method_kwargs={
                    "coords": self.__robust_estimation.matched_dst_keypoints_xy,
                },
            )

        # FILTERED outputs
        if "FILTERED" in self.output_level:
            self.__generate_outputs(
                level_dir="features",
                prefix="filtered",
                suffix="src.csv",
                save_method=self.__save_points,
                save_method_kwargs={
                    "coords": self.__robust_estimation.filtered_matched_src_keypoints_xy,
                },
            )
            self.__generate_outputs(
                level_dir="features",
                prefix="filtered",
                suffix="dst.csv",
                save_method=self.__save_points,
                save_method_kwargs={
                    "coords": self.__robust_estimation.filtered_matched_dst_keypoints_xy,
                },
            )

        # Instantiate 2D warping method
        self.__warp2D = Warping2D(self.__robust_estimation.robust_transform)

        # OVERLAP outputs
        if "OVERLAP" in self.output_level:
            self.__generate_outputs(
                level_dir="overlap",
                prefix="overlap",
                suffix="init.png",
                save_method=self.__save_img,
                save_method_kwargs={"img": draw_overlap_img([self.__dst_pre_process, self.__src_pre_process])},
            )
            self.__generate_outputs(
                level_dir="overlap",
                prefix="overlap",
                suffix="warped.png",
                save_method=self.__save_img,
                save_method_kwargs={
                    "img": draw_overlap_img(
                        [self.__dst_pre_process, self.__warp2D(self.__src_pre_process, order=3, cval=1.0)]
                    )
                },
            )
            self.__exec_times["warping"] = self.__warp2D.exec_time

        # LOGS outputs
        if "LOGS" in self.output_level:
            self.__generate_outputs(
                level_dir="logs",
                prefix="logs",
                suffix="exec_times.csv",
                save_method=self.__save_exec_time,
                save_method_kwargs={"times": self.__exec_times},
            )

    def __quantitative(
        self,
        src: np.ndarray,
        dst: np.ndarray,
        pixel_mapping: VectorType = FloatVector(x=1.0, y=1.0),
        metrics: List[str] = ["TRE", "RMSE"],
    ) -> Dict[str, Any]:
        warped_src = self.__warp2D(src)

        out = {
            "pixel_mapping_xy": pixel_mapping.xy,
            "src": src,
            "warped": warped_src,
            "dst": dst,
            "metrics": {"local": {}, "global": {}},
        }

        if "TRE" in metrics:
            # Initial TRE
            _tre = tre(src=src, dst=dst, pixel_mapping=pixel_mapping)
            out["metrics"]["local"]["init_tre"] = _tre

            # Post registration TRE
            _tre = tre(src=warped_src, dst=dst, pixel_mapping=pixel_mapping)
            out["metrics"]["local"]["tre"] = _tre

        if "RMSE" in metrics:
            # Initial TRE
            _rmse = (
                rmse(src=out["metrics"]["local"]["init_tre"], pixel_mapping=pixel_mapping)
                if out["metrics"]["local"].get("init_tre", None) is not None
                else rmse(src=src, dst=dst, pixel_mapping=pixel_mapping)
            )
            out["metrics"]["global"]["init_rmse"] = _rmse

            # Post registration TRE
            _rmse = (
                rmse(src=out["metrics"]["local"]["tre"], pixel_mapping=pixel_mapping)
                if out["metrics"]["local"].get("tre", None) is not None
                else rmse(src=warped_src, dst=dst, pixel_mapping=pixel_mapping)
            )
            out["metrics"]["global"]["rmse"] = _rmse

        return out

    def __qualitative(
        self,
        src_img: np.ndarray,
        dst_img: np.ndarray,
        src_pts: np.ndarray,
        dst_pts: np.ndarray,
        good_indices: Optional[np.ndarray] = None,
        plots: List[str] = ["InWaTa", "Matching"],
        cmap: colors.ListedColormap = generate_jzazbz_cmap(),
    ) -> Optional[Dict[str, Any]]:
        if good_indices is None or not list(good_indices):
            good_indices = np.ones((src_pts.shape[0],), bool)

        warped_src_pts = self.__warp2D(src_pts[good_indices, :])

        out = {}

        if "InWaTa" in plots:
            img = generate_standalone_figure(
                draw_init_warped_target,
                src_img=src_img,
                dst_img=dst_img,
                src_pts=src_pts[good_indices, :],
                warped_src_pts=warped_src_pts,
                dst_pts=dst_pts[good_indices, :],
                out_img="numpy",
            )
            out["inwata"] = img

        if "Matching" in plots:
            img = generate_standalone_figure(
                draw_matches,
                src_img=src_img,
                src_matched_kp=src_pts,
                dst_img=dst_img,
                dst_matched_kp=dst_pts,
                good_indices=good_indices,
                cmap=cmap,
                out_img="numpy",
            )
            out["matching"] = img

        return out

    def __describe_gt(self, img: np.ndarray, lndmrks: np.ndarray) -> None:
        def _augment(mask: np.ndarray, desc: np.ndarray) -> np.ndarray:
            augmented = np.zeros((mask.shape[0], desc.shape[1]))
            augmented[:] = np.NaN
            augmented[mask] = desc

            return augmented

        cv_lndmrks = cv2.KeyPoint_convert(lndmrks.tolist())

        descriptor = self.features_kwargs.get("descriptor")
        tmp, lndmrks_descriptors = descriptor(img, cv_lndmrks)

        indices = np.ones(lndmrks.shape[0], dtype=bool)
        updated_lndmrks = np.array([lndmrk.pt for lndmrk in tmp])
        if updated_lndmrks.shape != lndmrks.shape:
            skipped = 0
            for i, coords in enumerate(lndmrks):
                # To avoid getting out-of-bound due to a big size difference between updated_lndmrks and lndmrks
                if i - skipped == updated_lndmrks.shape[0]:
                    if i == indices.shape[0] - 1:
                        indices[i] = False
                    else:
                        indices[i:-1] = False
                    break

                if not np.allclose(coords, updated_lndmrks[i - skipped]):
                    indices[i] = False
                    skipped += 1

        lndmrks_descriptors = _augment(indices, lndmrks_descriptors)

        return indices, lndmrks_descriptors

    def __combine_lost_lndmrks(self, src_indices: np.ndarray, dst_indices: np.ndarray) -> np.ndarray:
        return src_indices * dst_indices

    def evaluate(
        self, extract_gt: Optional[Callable[[str, Optional[float], Optional[IntVector]], np.ndarray]] = None
    ) -> None:
        """Evaluate the experiment's performance both quantitatively and qualitatively in an
        unsupervised manner with the use of the corresponding keypoints and in a supervised manner
        with use of provided landmarks.

        :param extract_gt: A callable use to extract the ground truth if provided, defaults to None.
        :type extract_gt: Optional[Callable[[str, Optional[float], Optional[IntVector]], np.ndarray]], optional
        :raises TypeError: Triggered if the experiment has not yet been executed.
        """

        if not isinstance(self.__warp2D, Warping2D):
            raise TypeError(
                "The experiment has not been executed yet. Please execute it before evaluating "
                "its performance on the input."
            )

        self.__eval_results = {"unsupv": {}}

        # Compute unsupervised quantitative analysis
        pixel_mapping = self.src_wsi.um_per_pixel_at_mag(self.magnification)
        self.__eval_results["unsupv"]["quant"] = self.__quantitative(
            src=self.__robust_estimation.filtered_matched_src_keypoints_xy,
            dst=self.__robust_estimation.filtered_matched_dst_keypoints_xy,
            pixel_mapping=pixel_mapping,
            metrics=["TRE", "RMSE"],
        )

        # UNSUPV_QUANT outputs
        if "UNSUPV_QUANT" in self.output_level:
            self.__generate_outputs(
                level_dir="unsupv",
                prefix="quant",
                suffix="metrics.csv",
                save_method=self.__save_metrics,
                save_method_kwargs={
                    "quant": self.__eval_results["unsupv"]["quant"],
                },
            )

        # Compute unsupervised qualitative analysis
        self.__eval_results["unsupv"]["qual"] = self.__qualitative(
            src_img=self.__src_pre_process,
            dst_img=self.__dst_pre_process,
            src_pts=self.__robust_estimation.matched_src_keypoints_xy,
            dst_pts=self.__robust_estimation.matched_dst_keypoints_xy,
            good_indices=self.__robust_estimation.good_matches_indices,
            plots=["InWaTa", "Matching"],
        )

        # UNSUPV_QUAL outputs
        if "UNSUPV_QUAL" in self.output_level:
            self.__generate_outputs(
                level_dir="unsupv",
                prefix="qual",
                suffix="inwata.png",
                save_method=self.__save_img,
                save_method_kwargs={"img": self.__eval_results["unsupv"]["qual"]["inwata"]},
            )
            self.__generate_outputs(
                level_dir="unsupv",
                prefix="unsupv",
                suffix="matching.png",
                save_method=self.__save_img,
                save_method_kwargs={"img": self.__eval_results["unsupv"]["qual"]["matching"]},
            )

        if self.src_gt_path and self.dst_gt_path:
            self.__eval_results["supv"] = {}

            # Extract landmarks
            src_lndmrks = extract_gt(
                self.src_gt_path,
                factor=self.magnification / self.src_wsi.mag,
                offset=self.__src_pad.pad_before,
            )
            dst_lndmrks = extract_gt(
                self.dst_gt_path,
                factor=self.magnification / self.dst_wsi.mag,
                offset=self.__dst_pad.pad_before,
            )

            # Remove landmarks which have no correspondences in either one of the given ground truth
            nb_common = min(len(pts) for pts in [src_lndmrks, dst_lndmrks])
            src_lndmrks = src_lndmrks[:nb_common]
            dst_lndmrks = dst_lndmrks[:nb_common]

            # Compute the gt descriptors
            src_indices, src_desc = self.__describe_gt(self.__src_pre_process, src_lndmrks)
            dst_indices, dst_desc = self.__describe_gt(self.__dst_pre_process, dst_lndmrks)

            # Get the mask to remove the lost landmarks due to the description
            kept_lndmrks_mask = self.__combine_lost_lndmrks(src_indices, dst_indices)

            # Compute supervised quantitative analysis
            self.__eval_results["supv"]["quant"] = self.__quantitative(
                src=src_lndmrks,
                dst=dst_lndmrks,
                pixel_mapping=pixel_mapping,
                metrics=["TRE", "RMSE"],
            )

            # SUPV_QUANT outputs
            if "SUPV_QUANT" in self.output_level:
                self.__generate_outputs(
                    level_dir="supv",
                    prefix="quant",
                    suffix="metrics.csv",
                    save_method=self.__save_metrics,
                    save_method_kwargs={
                        "quant": self.__eval_results["supv"]["quant"],
                        "gt_path": {"dst_gt_path": self.dst_gt_path, "src_gt_path": self.src_gt_path},
                    },
                )
                self.__generate_outputs(
                    level_dir="supv",
                    prefix="desc",
                    suffix="src.csv",
                    save_method=self.__save_points,
                    save_method_kwargs={
                        "coords": src_lndmrks[kept_lndmrks_mask],
                        "descriptors": src_desc[kept_lndmrks_mask],
                    },
                )
                self.__generate_outputs(
                    level_dir="supv",
                    prefix="desc",
                    suffix="dst.csv",
                    save_method=self.__save_points,
                    save_method_kwargs={
                        "coords": dst_lndmrks[kept_lndmrks_mask],
                        "descriptors": dst_desc[kept_lndmrks_mask],
                    },
                )

            # Compute supervised qualitative analysis
            self.__eval_results["supv"]["qual"] = self.__qualitative(
                src_img=self.__src_pre_process,
                dst_img=self.__dst_pre_process,
                src_pts=src_lndmrks,
                dst_pts=dst_lndmrks,
                plots=["InWaTa"],
            )

            # SUPV_QUAL outputs
            if "SUPV_QUAL" in self.output_level:
                self.__generate_outputs(
                    level_dir="supv",
                    prefix="supv",
                    suffix="inwata.png",
                    save_method=self.__save_img,
                    save_method_kwargs={"img": self.__eval_results["supv"]["qual"]["inwata"]},
                )
