import logging
import os
from typing import Any, Callable, Dict, List, Optional, Tuple

import numpy as np
import pandas as pd
from openwholeslide import IntVector, WholeSlide

logger = logging.getLogger()


def scale_ground_truth(df: pd.DataFrame, factor: Optional[float] = None) -> pd.DataFrame:
    """Scale the coordinates of landmarks with a given factor.

    :param df: The dataframe containing the landmarks.
    :type df: pd.DataFrame
    :param factor: The scaling factor, defaults to None.
    :type factor: Optional[float], optional
    :return: The scaled landmarks.
    :rtype: pd.DataFrame
    """

    if not factor or factor == 1.0:
        logger.info("No scaling.")
        return df

    logger.info(f"Landmarks scaled of a {factor=}.")
    return df * factor


def extract_set_list(filename: str) -> List[Tuple[str]]:
    """Extract the list of pair that constitutes the set.

    :param filename: The path to the .txt file containing the list of pairs.
    Each pair should be written on a new line with the following
    structure: path/from/basepath/to/src, path/from/basepath/to/dst
    :type filename: str
    :return: The list of pairs in the (src, dst) order
    :rtype: List[Tuple[str]]
    """

    with open(filename, "r", encoding="utf-8") as fp:
        tmp = [line.rstrip() for line in fp]

    return [tuple(elem.split(", ")) for elem in tmp]


class GenericRegistrationDataset:
    """Generic class to load a registration dataset."""

    def __init__(
        self,
        basepath: Optional[str],
        dev_list: str = "dev_set.txt",
        test_list: str = "test_set.txt",
        ground_truth_extractor: Optional[Callable[[str, Optional[float], Optional[IntVector]], np.ndarray]] = None,
    ) -> None:
        """GenericRegistrationDataset constructor.

        :param basepath: The path to the dataset.
        :type basepath: Optional[str]
        :param dev_list: The relative path from the basepath to the text file for the dev_set that contains the
        pairs of images to register, defaults to "dev_set.txt".
        :type dev_list: str, optional
        :param test_list: The relative path from the basepath to the text file for the test_set that contains the
        pairs of images to register, defaults to "test_set.txt".
        :type test_list: str, optional
        :param ground_truth_extractor: The callable that will be used to extract the ground truth, defaults to None.
        :type ground_truth_extractor: Optional[Callable[[str, Optional[float], Optional[IntVector]], np.ndarray]], optional
        """

        self.basepath = basepath
        self.dev_list = extract_set_list(os.path.join(basepath, dev_list)) if basepath else []
        self.test_list = extract_set_list(os.path.join(basepath, test_list)) if basepath else []
        self.extractor = ground_truth_extractor

        self._dev_set = {}
        self._test_set = {}

    @property
    def dev_set(self) -> Dict[int, Any]:
        if not self._dev_set:
            self._load_dev_set()
        return self._dev_set

    @property
    def test_set(self) -> Dict[int, Any]:
        if not self._test_set:
            self._load_test_set()
        return self._test_set

    def _load_dev_set(self) -> None:
        for i, pair in enumerate(self.dev_list):
            self._dev_set[i] = {
                "src": WholeSlide(os.path.join(self.basepath, pair[0])),
                "dst": WholeSlide(os.path.join(self.basepath, pair[1])),
                "src_gt": None,
                "dst_gt": None,
            }

    def _load_test_set(self) -> None:
        for i, pair in enumerate(self.test_list):
            self._dev_set[i] = {
                "src": WholeSlide(os.path.join(self.basepath, pair[0])),
                "dst": WholeSlide(os.path.join(self.basepath, pair[1])),
                "src_gt": None,
                "dst_gt": None,
            }


class CompleteDataset(GenericRegistrationDataset):
    """Complete dataset constructed from the combination of multiple datasets."""

    def __init__(self, datasets: List[GenericRegistrationDataset]) -> None:
        """CompleteDataset constructor.

        :param datasets: The datasets that will compose the CompleteDataset instance.
        :type datasets: List[GenericRegistrationDataset]
        """

        super().__init__(basepath=None)
        self.datasets = datasets

    def _load_dev_set(self) -> None:
        for ds in self.datasets:
            next_key_index = len(self._dev_set.keys())

            for key, value in ds.dev_set.items():
                value["gt_extractor"] = ds.extractor
                self._dev_set[next_key_index + key] = value

    def _load_test_set(self) -> None:
        for ds in self.datasets:
            next_key_index = len(self._test_set.keys())

            for key, value in ds.test_set.items():
                self._test_set[next_key_index + key] = value
