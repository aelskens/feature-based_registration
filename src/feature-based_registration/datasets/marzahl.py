import ast
import logging
import os
from typing import Optional

import numpy as np
import pandas as pd
from datasets.generic import GenericRegistrationDataset, scale_ground_truth
from openwholeslide import FloatVector, IntVector, WholeSlide

logger = logging.getLogger()


def extract_MSSC_ground_truth(
    path: str, factor: Optional[float] = None, offset: Optional[IntVector] = None
) -> np.ndarray:
    """Extract the MSSC landmarks, scale and offset them if needed.

    :param path: The path to the .csv containing the ground truth.
    :type path: str
    :param factor: The scaling factor, defaults to None.
    :type factor: Optional[float], optional
    :param offset: The offset to apply to the landmarks, defaults to None.
    :type offset: Optional[IntVector], optional
    :return: The scaled and offset landmarks coordinates as (x, y).
    :rtype: np.ndarray
    """

    tmp = pd.read_csv(path)

    # Get center of bbox
    bboxes = [ast.literal_eval(bbox) for bbox in tmp["vector"]]
    landmarks = [
        {"x": bbox["x1"] + (bbox["x2"] - bbox["x1"]) / 2, "y": bbox["y1"] + (bbox["y2"] - bbox["y1"]) / 2}
        for bbox in bboxes
    ]
    df = pd.DataFrame(landmarks)

    df = scale_ground_truth(df, factor)

    out = np.empty((len(df), 2))
    out[:, 0] = df["x"] if not offset else df["x"] + offset.x
    out[:, 1] = df["y"] if not offset else df["y"] + offset.y

    return out


class MSSCDataset(GenericRegistrationDataset):
    """MSSC dataset loader."""

    def __init__(
        self,
        basepath: str,
        dev_list: str = "dev_set.txt",
        test_list: str = "test_set.txt",
        full_scale: float = 40.0,
        pixel_resolution: FloatVector = FloatVector(x=0.25, y=0.25),
    ) -> None:
        """MSSCDataset constructor.

        :param basepath: The path to the dataset.
        :type basepath: Optional[str]
        :param dev_list: The relative path from the basepath to the text file for the dev_set that contains the
        pairs of images to register, defaults to "dev_set.txt".
        :type dev_list: str, optional
        :param test_list: The relative path from the basepath to the text file for the test_set that contains the
        pairs of images to register, defaults to "test_set.txt".
        :type test_list: str, optional
        :param full_scale: The magnification of the full scale level, defaults to 40.0.
        :type full_scale: float, optional
        :param pixel_resolution: The pixel resolutions in the x and y directions.
        :type pixel_resolution: FloatVector, optional
        """

        super().__init__(basepath, dev_list, test_list, ground_truth_extractor=extract_MSSC_ground_truth)
        self.mag: float = full_scale
        self.mpp: FloatVector = pixel_resolution

    def _load_dev_set(self) -> None:
        for i, pair in enumerate(self.dev_list):
            self._dev_set[i] = {
                "src": WholeSlide(os.path.join(self.basepath, pair[0])),
                "dst": WholeSlide(os.path.join(self.basepath, pair[1])),
                "src_gt": os.path.join(
                    self.basepath, os.path.splitext(pair[0].replace("images", "landmarks"))[0] + ".csv"
                ),
                "dst_gt": os.path.join(
                    self.basepath, os.path.splitext(pair[1].replace("images", "landmarks"))[0] + ".csv"
                ),
            }
            self._dev_set[i]["src"].mag = self.mag
            self._dev_set[i]["dst"].mag = self.mag
            self._dev_set[i]["src"].mpp = self.mpp
            self._dev_set[i]["dst"].mpp = self.mpp

    def _load_test_set(self) -> None:
        for i, pair in enumerate(self.test_list):
            self._test_set[i] = {
                "src": WholeSlide(os.path.join(self.basepath, pair[0])),
                "dst": WholeSlide(os.path.join(self.basepath, pair[1])),
                "src_gt": os.path.join(
                    self.basepath, os.path.splitext(pair[0].replace("images", "landmarks"))[0] + ".csv"
                ),
                "dst_gt": os.path.join(
                    self.basepath, os.path.splitext(pair[0].replace("images", "landmarks"))[0] + ".csv"
                ),
            }
            self._test_set[i]["src"].mag = self.mag
            self._test_set[i]["dst"].mag = self.mag
            self._test_set[i]["src"].mpp = self.mpp
            self._test_set[i]["dst"].mpp = self.mpp


class ISCDataset(GenericRegistrationDataset):
    """ISC dataset loader."""

    def __init__(
        self,
        basepath: str,
        dev_list: str = "dev_set.txt",
        test_list: str = "test_set.txt",
        full_scale: float = 40.0,
        pixel_resolution: FloatVector = FloatVector(x=0.25, y=0.25),
    ) -> None:
        """ISCDataset constructor.

        :param basepath: The path to the dataset.
        :type basepath: Optional[str]
        :param dev_list: The relative path from the basepath to the text file for the dev_set that contains the
        pairs of images to register, defaults to "dev_set.txt".
        :type dev_list: str, optional
        :param test_list: The relative path from the basepath to the text file for the test_set that contains the
        pairs of images to register, defaults to "test_set.txt".
        :type test_list: str, optional
        :param full_scale: The magnification of the full scale level, defaults to 40.0.
        :type full_scale: float, optional
        :param pixel_resolution: The pixel resolutions in the x and y directions.
        :type pixel_resolution: FloatVector, optional
        """

        super().__init__(basepath, dev_list, test_list, ground_truth_extractor=extract_MSSC_ground_truth)
        self.mag: float = full_scale
        self.mpp: FloatVector = pixel_resolution

    def _load_dev_set(self) -> None:
        for i, pair in enumerate(self.dev_list):
            self._dev_set[i] = {
                "src": WholeSlide(os.path.join(self.basepath, pair[0])),
                "dst": WholeSlide(os.path.join(self.basepath, pair[1])),
                "src_gt": os.path.join(
                    self.basepath, os.path.splitext(pair[0].replace("images", "landmarks"))[0] + ".csv"
                ),
                "dst_gt": os.path.join(
                    self.basepath, os.path.splitext(pair[1].replace("images", "landmarks"))[0] + ".csv"
                ),
            }
            self._dev_set[i]["src"].mag = self.mag
            self._dev_set[i]["dst"].mag = self.mag
            self._dev_set[i]["src"].mpp = self.mpp
            self._dev_set[i]["dst"].mpp = self.mpp

    def _load_test_set(self) -> None:
        for i, pair in enumerate(self.test_list):
            self._test_set[i] = {
                "src": WholeSlide(os.path.join(self.basepath, pair[0])),
                "dst": WholeSlide(os.path.join(self.basepath, pair[1])),
                "src_gt": os.path.join(
                    self.basepath, os.path.splitext(pair[0].replace("images", "landmarks"))[0] + ".csv"
                ),
                "dst_gt": os.path.join(
                    self.basepath, os.path.splitext(pair[0].replace("images", "landmarks"))[0] + ".csv"
                ),
            }
            self._test_set[i]["src"].mag = self.mag
            self._test_set[i]["dst"].mag = self.mag
            self._test_set[i]["src"].mpp = self.mpp
            self._test_set[i]["dst"].mpp = self.mpp
