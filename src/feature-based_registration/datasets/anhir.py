import logging
import os
import re
from typing import Optional

import numpy as np
import pandas as pd
from datasets.generic import GenericRegistrationDataset, scale_ground_truth
from openwholeslide import ImageWSI, IntVector, WholeSlide

logger = logging.getLogger()


def extract_ANHIR_ground_truth(
    path: str, factor: Optional[float] = None, offset: Optional[IntVector] = None
) -> np.ndarray:
    """Extract the ANHIR landmarks, scale and offset them if needed.

    :param path: The path to the .csv containing the ground truth.
    :type path: str
    :param factor: The scaling factor, defaults to None.
    :type factor: Optional[float], optional
    :param offset: The offset to apply to the landmarks, defaults to None.
    :type offset: Optional[IntVector], optional
    :return: The scaled and offset landmarks coordinates as (x, y).
    :rtype: np.ndarray
    """

    df = pd.read_csv(path)
    df = scale_ground_truth(df, factor)

    out = np.empty((len(df), 2))
    out[:, 0] = df["X"] if not offset else df["X"] + offset.x
    out[:, 1] = df["Y"] if not offset else df["Y"] + offset.y

    return out


class ANHIRDataset(GenericRegistrationDataset):
    """ANHIR dataset loader."""

    TISSUES_METADATA = {
        "breast": {
            "magnification": 40.0,
            "x_resolution": 0.2528,
            "y_resolution": 0.2528,
        },
        "COAD": {
            "magnification": 20.0,
            "x_resolution": 0.468,
            "y_resolution": 0.468,
        },
        "gastric": {
            "magnification": 40.0,
            "x_resolution": 0.2528,
            "y_resolution": 0.2528,
        },
        "kidney": {
            "magnification": 40.0,
            "x_resolution": 0.2528,
            "y_resolution": 0.2528,
        },
        "lung-lesion": {
            "magnification": 40.0,
            "x_resolution": 0.174,
            "y_resolution": 0.174,
        },
        "lung-lobes": {
            "magnification": 10.0,
            "x_resolution": 1.274,
            "y_resolution": 1.274,
        },
        # "mammary-gland": {
        #     "magnification": 20.0,
        #     "x_resolution": 0.2528,
        #     "y_resolution": 0.2528,
        # },
        "mice-kidney": {
            "magnification": 40.0,
            "x_resolution": 0.227,
            "y_resolution": 0.227,
        },
    }

    def __init__(
        self, basepath: Optional[str], dev_list: str = "dev_set.txt", test_list: str = "test_set.txt"
    ) -> None:
        """ANHIRDataset constructor.

        :param basepath: The path to the dataset.
        :type basepath: Optional[str]
        :param dev_list: The relative path from the basepath to the text file for the dev_set that contains the
        pairs of images to register, defaults to "dev_set.txt".
        :type dev_list: str, optional
        :param test_list: The relative path from the basepath to the text file for the test_set that contains the
        pairs of images to register, defaults to "test_set.txt".
        :type test_list: str, optional
        """

        super().__init__(basepath, dev_list, test_list, ground_truth_extractor=extract_ANHIR_ground_truth)

    def _load_dev_set(self) -> None:
        for i, pair in enumerate(self.dev_list):
            tissue = pair[0].split("/")[1].split("_")[0]
            tissue_info = self.TISSUES_METADATA[tissue]

            scale_pc = float(re.search(r"scale-\d\.?\d{,2}", pair[0])[0].split("-")[-1])
            img_mag = scale_pc * (tissue_info["magnification"] / 100)

            metadata = {
                "mag": img_mag,
                "mpp_x": tissue_info["x_resolution"] * tissue_info["magnification"] / img_mag,
                "mpp_y": tissue_info["y_resolution"] * tissue_info["magnification"] / img_mag,
            }

            self._dev_set[i] = {
                "src": WholeSlide(
                    os.path.join(self.basepath, pair[0]), reader_cls=ImageWSI, additional_metadata=metadata
                ),
                "dst": WholeSlide(
                    os.path.join(self.basepath, pair[1]), reader_cls=ImageWSI, additional_metadata=metadata
                ),
                "src_gt": os.path.join(
                    self.basepath, os.path.splitext(pair[0].replace("images", "landmarks"))[0] + ".csv"
                ),
                "dst_gt": os.path.join(
                    self.basepath, os.path.splitext(pair[1].replace("images", "landmarks"))[0] + ".csv"
                ),
            }

    def _load_test_set(self) -> None:
        for i, pair in enumerate(self.test_list):
            tissue = pair[0].split("/")[1].split("_")[0]
            tissue_info = self.TISSUES_METADATA[tissue]

            metadata = {
                "mag": float(re.search(r"scale-\d\.?\d{,2}", pair[0])[0].split("-")[-1])
                * (tissue_info["magnification"] / 100),
                "mpp_x": tissue_info["x_resolution"],
                "mpp_y": tissue_info["y_resolution"],
            }

            self._test_set[i] = {
                "src": WholeSlide(
                    os.path.join(self.basepath, pair[0]), reader_cls=ImageWSI, additional_metadata=metadata
                ),
                "dst": WholeSlide(
                    os.path.join(self.basepath, pair[1]), reader_cls=ImageWSI, additional_metadata=metadata
                ),
                "src_gt": os.path.join(
                    self.basepath, os.path.splitext(pair[0].replace("images", "landmarks"))[0] + ".csv"
                ),
                "dst_gt": os.path.join(
                    self.basepath, os.path.splitext(pair[1].replace("images", "landmarks"))[0] + ".csv"
                ),
            }
