import argparse
import csv
import os
from typing import Any, Callable, Dict, List, Optional, Tuple, Union

import numpy as np
import pandas as pd
from extend_experiments import DESC_DIST_N_TYPE, generate_preproc_exp_list
from metrics.common import (
    compute_descriptive_statistics,
    compute_interquartile,
    friedman_test,
    nemenyi_multitest,
)
from metrics.descriptive import normalize_ranks
from viz.wsi import draw_boxplot, generate_standalone_figure


def create_parser() -> argparse.ArgumentParser:
    """Create the arg parser.
    For addition information: https://docs.python.org/3/library/argparse.html

    :return parser: The arg parser
    :rtype: argparse.ArgumentParser
    """

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-e",
        "--exps_path",
        type=str,
        required=True,
        default=None,
        help="The path to the experiments root directory.",
    )
    parser.add_argument(
        "-o",
        "--outpath",
        type=str,
        required=False,
        default=None,
        help="The path to the directory where the analysis should be saved. "
        "Only needed if the analysis should not be saved at the experiments root directory.",
    )

    return parser


def _extract_exp_normalized_ranks(exp_path: str) -> np.ndarray:
    """Extract the landmarks normalized matching ranks for a given experiment.

    :param exp_path: The path to the given experiment.
    :type exp_path: str
    :return: The landmarks normalized matching ranks for the given experiment.
    :rtype: np.ndarray
    """

    ranks_supv_path = os.path.join(exp_path, "supv/ranks_lndmrks_desc.csv")
    df = pd.read_csv(ranks_supv_path, index_col=0)

    return normalize_ranks(np.array(df["dst_lndmrks_matching_rank"]))


def _extract_exp_registration_metric(exp_path: str, metric: str = "rmse") -> np.ndarray:
    """Extract a specified metric for a given experiment.

    :param exp_path: The path to the given experiment.
    :type exp_path: str
    :param metric: The metric to extract, defaults to "rmse".
    :type metric: str, optional
    :return: The desired metric for the given experiment.
    :rtype: np.ndarray
    """

    quant_supv_path = os.path.join(exp_path, "supv/quant_metrics_global.csv")
    df = pd.read_csv(quant_supv_path, index_col=0)

    return np.array(df[metric])


def extract_metric(
    preproc_dict: Dict[str, List[str]],
    extraction_method: Callable = _extract_exp_registration_metric,
    **extraction_method_kwargs: Any,
) -> List[float]:
    """Extract and aggregate (per descriptor) the given metric for all the experiments
    with a given pre-processing.

    :param preproc_dict: The collection of experiments lists per descriptors for
    the given pre-processing.
    :type preproc_dict: Dict[str, List[str]]
    :param extraction_method: The method used to extract a given metric from the experiments
    outputs, defaults to _extract_exp_registration_metric.
    :type extraction_method: Callable, optional
    :return out: The aggregation per descriptor of the given metric throughout the entire
    dataset.
    :rtype: List[float]
    """

    out = [[] for i in range(len(preproc_dict.keys()))]
    for key, value in preproc_dict.items():
        for exp_path in sorted(value):
            normalized_ranks = extraction_method(exp_path, **extraction_method_kwargs)
            out[list(DESC_DIST_N_TYPE.keys()).index(key)] += list(normalized_ranks)

    return out


def compute_dstrb_descriptive_statistics(
    distributions: Dict[str, List[float]], prefix: str = "", string_format: str = "float_2"
) -> Dict[str, Union[int, float, Tuple[float], Tuple[int]]]:
    """Compute the distributions descriptive statistics.

    :param distributions: The distributions which will be described.
    :type distributions: Dict[str, List[float]]
    :param prefix: The prefix for the distributions, defaults to "".
    :type prefix: str, optional
    :param string_format: To which precision the descriptive statistics should be displayed in.
    It can either be ``int`` or ``float_x`` where x is the number of decimals, defaults to float_2.
    :type string_format: str, optional
    :return: The distributions descriptive statistics.
    :rtype: Dict[str, Union[int, float, Tuple[float], Tuple[int]]]
    """

    def __format_median(m: int | float, string_format: str) -> str:
        to_display = ""
        if string_format == "int":
            to_display = str(round(m))
        elif string_format == "float":
            to_display = str(round(m, 2))
        elif "float" in string_format:
            ndecimals = string_format.split("_")[-1]

            try:
                ndecimals = int(ndecimals)
            except ValueError:
                ndecimals = 2

            to_display = str(round(m, ndecimals))
        else:
            raise ValueError(
                f"Bad format ({string_format}), the possible values are: int, float, float_x (where x "
                "is the precision to which the median will be rounded)."
            )

        return to_display

    for k in distributions:
        to_compute = [np.median, compute_interquartile]
        desc_stats = compute_descriptive_statistics(distributions[k], to_compute)
        distributions[k] = {
            f"{prefix}median": __format_median(desc_stats[0], string_format),
            f"{prefix}interquartile_range": f"[{__format_median(desc_stats[1][0], string_format)}-{__format_median(desc_stats[1][1], string_format)}]",
        }

    return distributions


def merge_dict_to_df(*args: Any, partial_keys: List[str] = ["Init"]) -> pd.DataFrame:
    """Merge dictionaries which have the same keys and convert the result to a pandas.DataFrame.

    :raises ValueError: Triggers when no dictionaries are provided.
    :return: The merged dictionary converted to a pandas.DataFrame.
    :rtype: pd.DataFrame
    """

    if not args:
        raise ValueError(f"Missing input: {args}.")

    if len(args) == 1:
        return args

    def __add_partial_keys(incomplete: Dict[str, Any], partial_keys: List[str] = ["Init"]) -> Dict[str, Any]:
        for p_k in partial_keys:
            if p_k not in incomplete.keys():
                incomplete[p_k] = {}

        return incomplete

    merged = __add_partial_keys(args[0], partial_keys)
    for elem in args[1:]:
        for k in merged.keys():
            if not elem.get(k, None):
                continue

            merged[k] = {**merged[k], **elem[k]}

    return pd.DataFrame(merged).T


def save_statistical_tests(
    filepath: str, friedman_pval: float, dstrb_labels: List[str], nemenyi_pvals: np.ndarray
) -> None:
    """Save the statistical tests results in a .csv file.

    :param filepath: The path to the .csv file.
    :type filepath: str
    :param friedman_pval: The Friedman's test p-value.
    :type friedman_pval: float
    :param dstrb_labels: The labels of the different distributions.
    :type dstrb_labels: List[str]
    :param nemenyi_pvals: The pairwise Nemenyi post-hoc test p-values.
    :type nemenyi_pvals: np.ndarray
    """

    with open(filepath, mode="w", encoding="utf-8") as fp:
        writer = csv.writer(fp, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)

        writer.writerow(["Friedman's test p-value"])
        writer.writerow([friedman_pval])

        for _ in range(2):
            writer.writerow([])

        writer.writerow(["Nemenyi post-hoc test", *dstrb_labels])
        for ind, nem_pval in enumerate(nemenyi_pvals):
            writer.writerow([dstrb_labels[ind], *nem_pval])


def main(exps_path: str, outpath: Optional[str] = None) -> None:
    """Generate the tables and figures used in the analysis.

    :param exps_path: The path to the experiments root directory.
    :type exps_path: str
    :param outpath: The path to the directory where the analysis should be saved, it will be
    saved under a subdirectory named 'experiments_analysis'. The experiments root directory
    will be selected by default, defaults to None.
    :type outpath: Optional[str], optional
    """

    if outpath:
        analysis_path = os.path.join(outpath, "experiments_analysis")
    else:
        analysis_path = os.path.join(exps_path, "experiments_analysis")

    if not os.path.exists(analysis_path):
        os.mkdir(analysis_path)

    for preproc in os.listdir(exps_path):
        preproc_path = os.path.join(exps_path, preproc)
        if not os.path.isdir(preproc_path) or "experiments_analysis" == preproc:
            continue

        preproc_dict = generate_preproc_exp_list(preproc_path)

        analysis_preproc_path = os.path.join(analysis_path, preproc)
        if not os.path.exists(analysis_preproc_path):
            os.mkdir(analysis_preproc_path)

        # Save the normalized ranks distributions boxplot
        ranks_dstrb = {
            k: v for k, v in zip(DESC_DIST_N_TYPE.keys(), extract_metric(preproc_dict, _extract_exp_normalized_ranks))
        }
        generate_standalone_figure(
            draw_boxplot,
            out_img=os.path.join(analysis_preproc_path, "descriptors_matching_normalized_ranks.png"),
            distributions=list(ranks_dstrb.values()),
            distrib_labels=ranks_dstrb.keys(),
            xlabel="Descriptors",
            ylabel="Normalized ranks",
        )

        # Save the RMSE distributions boxplot
        # Add the initial RMSE to the distributions
        init_dict = {list(DESC_DIST_N_TYPE.keys())[0]: preproc_dict[list(DESC_DIST_N_TYPE.keys())[0]]}
        rmse_dstrb = {
            k: v
            for k, v in zip(
                ["Init"] + list(DESC_DIST_N_TYPE.keys()),
                extract_metric(init_dict, _extract_exp_registration_metric, metric="init_rmse")
                + extract_metric(preproc_dict, _extract_exp_registration_metric, metric="rmse"),
            )
        }
        generate_standalone_figure(
            draw_boxplot,
            out_img=os.path.join(analysis_preproc_path, "descriptors_rmse.png"),
            distributions=list(rmse_dstrb.values()),
            distrib_labels=["Init"] + list(DESC_DIST_N_TYPE.keys()),
            xlabel="Descriptors",
            ylabel="RMSE [microns]",
            log=True,
            displayed_median_precision="int",
        )

        # Save Table containing the descriptive statistics per descriptor for both the normalized
        # ranks and the RMSE distributions
        df = merge_dict_to_df(
            compute_dstrb_descriptive_statistics(ranks_dstrb.copy(), prefix="Normalized-ranks_"),
            compute_dstrb_descriptive_statistics(rmse_dstrb.copy(), prefix="RMSE_", string_format="int"),
        )
        df = df.reindex(["Init"] + list(DESC_DIST_N_TYPE.keys()))
        df.to_csv(os.path.join(analysis_preproc_path, "matching_normalized_ranks_N_rmse_summary.csv"))

        # Save Table with the results of the Friedman and Nemenyi tests
        ranks_fried = friedman_test(*ranks_dstrb.values())
        ranks_nemenyi = nemenyi_multitest(ranks_fried[-1])
        save_statistical_tests(
            filepath=os.path.join(analysis_preproc_path, "matching_normalized_ranks_statistical_tests.csv"),
            friedman_pval=ranks_fried[1],
            dstrb_labels=list(ranks_dstrb.keys()),
            nemenyi_pvals=ranks_nemenyi,
        )
        rmse_fried = friedman_test(*rmse_dstrb.values())
        rmse_nemenyi = nemenyi_multitest(rmse_fried[-1])
        save_statistical_tests(
            filepath=os.path.join(analysis_preproc_path, "rmse_statistical_tests.csv"),
            friedman_pval=rmse_fried[1],
            dstrb_labels=list(rmse_dstrb.keys()),
            nemenyi_pvals=rmse_nemenyi,
        )


if __name__ == "__main__":
    parser = create_parser()
    args = parser.parse_args()

    main(exps_path=args.exps_path, outpath=args.outpath)
